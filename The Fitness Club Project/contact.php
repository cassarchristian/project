<?php 

session_start();

?>

<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Contact
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <img class="img-responsive" src="images/contactusimg.png" style="border-radius: 6px;"alt="">
            </div>
        
            <div class="col-md-6">
                <h3 class="defaultfontbold">Contact Details</h3>
                <p class="defaultfont">
                    The Fitness Club, <br>Fleur De Lys, Birkirkara, BKR1480<br>
                    <a class="openinghours" href="location.php">Opening Hours</a>
                </p>
                
                <b><p class="defaultfont"><i class="fa fa-phone" style="font-size: 20px; color: #FF1010;"></i> 
                    +365 79123456</p></b>
                
                <b><p class="defaultfont"><i class="fa fa-envelope" style="font-size: 20px; color: #FF1010;"></i> 
                    thefitnesscluborg@gmail.com</p></b>
     
                <ul class="list-unstyled list-inline list-social-icons">
                    <li>
                        <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-square" aria-hidden="true" style="color: #FF1010; margin-right: 5px; font-size: 25px;"></i></a>
                    </li>
                    <li>
                        <a href="https://www.twitter.com" target="_blank"><i class="fab fa-twitter-square" aria-hidden="true" style="color: #FF1010; margin-right: 5px;font-size: 25px;"></i></a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram" aria-hidden="true" style="color: #FF1010;font-size: 25px;"></i></a>
                    </li> 
                </ul>
            </div>
        </div>
        

        
        <div class="row"style="margin-top: 20px;">
            <div class="col-lg-12">
                <h4 class="page-header defaultfontbold">Contact via Form
                    <br><small class="defaultfont">Fill in the form below to send a message</small>
                </h4>
            </div>
            
            <div class="col-md-10">
                <form id="contact-form" method="post" action="contact.php" role="form">

                <div class="messages"></div>

                <div class="controls defaultfont">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_name">First Name *</label>
                                <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your name *" required="required" data-error="Firstname is required.">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_lastname">Last Name *</label>
                                <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your surname *" required="required" data-error="Lastname is required.">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_phone">Phone</label>
                                <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                          <label for="sel1">Subject *</label>
                          <select class="form-control" name="subject" id="sel1">
                            <option value="Suggestion">Suggestion</option>
                            <option value="Feedback">Feedback</option>
                            <option value="Student Offer">Student Offers</option>
                            <option value="Help">Help</option>
                          </select>
                        </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="form_message">Message *</label>
                                <textarea id="form_message" name="message" class="form-control" placeholder="Leave a Message *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" name="submit" class="btn defaultbutton btn-send" value="Send Message">
                        </div>
                    </div>
                    </div>
            </form>
            </div>
        </div>
    
        
    </div>
    
    <?php 
        if(isset($_POST['submit'])){
            $to = "thefitnesscluborg@gmail.com"; // this is your Email address
            $from = mysqli_real_escape_string($conn,trim($_POST['email'])); // this is the sender's Email address
            $first_name = mysqli_real_escape_string($conn,trim($_POST['name']));
            $last_name = mysqli_real_escape_string($conn,trim($_POST['surname']));
            $contact_number = mysqli_real_escape_string($conn,trim($_POST['phone']));
            $subject = mysqli_real_escape_string($conn,trim($_POST['subject']));
            $subject2 = "The Fitness Club - Contact Form Submission";
            $messagewithcontact = $first_name . " " . $last_name . " sent a message:" . "\n\n" . $_POST['message']. "\n\n". "Contact Number: " . $contact_number;
            $messagewithoutcontact = $first_name . " " . $last_name . " sent a message:" . "\n\n" . $_POST['message'];
            $message = "Hello";
            $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $_POST['message'];

            $headers = "From:" . $from;
            $headers2 = "From:" . $to;
            
            if (empty($contact_number)) {
                mail($to,$subject,$messagewithoutcontact,$headers);
                sleep(2);
                echo "<meta http-equiv='refresh' content=\"0; url=emailsent.php\">";
                exit();
            }
            else{
                if(!is_numeric($contact_number) && !strlen($contact_number) == 8)
                {
                    echo "<div class='container'>
                            <div class='row'>
                            <h3 class='defaultfontbold'>Error: Message could not be sent.</h3>
                            <p class='defaultfontbold' style='color: red'>Phone Number is invalid, please try again!<p>
                            </div>
                          </div>";
                }
                else if(is_numeric($first_name)){
                    echo "<div class='container'>
                            <div class='row'>
                            <h3 class='defaultfontbold'>Error: Message could not be sent.</h3>
                            <p class='defaultfontbold' style='color: red'>First name is numeric, please try again!<p>
                            </div>
                          </div>";
                    
                }
                else if(is_numeric($last_name)){
                    echo "<div class='container'>
                            <div class='row'>
                            <h3 class='defaultfontbold'>Error: Message could not be sent.</h3>
                            <p class='defaultfontbold' style='color: red'>Last name is numeric, please try again!<p>
                            </div>
                          </div>";
                    
                }
                
                else
                {
                    mail($to,$subject,$messagewithcontact,$headers);
                    sleep(2);
                    echo "<meta http-equiv='refresh' content=\"0; url=emailsent.php\">";
                    exit();
                }
            }
            

            }
        ?>
    

       
            <?php
                if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
                {
                    include 'footerlogged.php';

                }else{

                    include 'footer.php';

                }
            ?>





        
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>