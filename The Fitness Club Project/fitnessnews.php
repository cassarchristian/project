<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Latest Fitness News
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
                
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="images/fitnessnews.png" alt=""  style="border-top-left-radius: 7px; border-top-right-radius: 7px;">
            </div>
        </div>
                
        <div class="row" style="margin-top: 10px;">
            <div class="col-lg-12 col-md-4 col-sm-4">
                
                    <!-- start sw-rss-feed code --> 
                    <script type="text/javascript"> 
                    <!-- 
                    rssfeed_url = new Array(); 
                    rssfeed_url[0]="https://healthworksfitness.com/feed/"; rssfeed_url[1]="http://www.bornfitness.com/feed/";  
                    rssfeed_frame_width="100%"; 
                    rssfeed_frame_height="700"; 
                    rssfeed_scroll="on"; 
                    rssfeed_scroll_step="6"; 
                    rssfeed_scroll_bar="off"; 
                    rssfeed_target="_blank"; 
                    rssfeed_font_size="12"; 
                    rssfeed_font_face=""; 
                    rssfeed_border="on"; 
                    rssfeed_css_url="https://feed.surfing-waves.com/css/style4.css"; 
                    rssfeed_title="on"; 
                    rssfeed_title_name=""; 
                    rssfeed_title_bgcolor="#3366ff"; 
                    rssfeed_title_color="#fff"; 
                    rssfeed_title_bgimage=""; 
                    rssfeed_footer="off"; 
                    rssfeed_footer_name="rss feed"; 
                    rssfeed_footer_bgcolor="#fff"; 
                    rssfeed_footer_color="#333"; 
                    rssfeed_footer_bgimage=""; 
                    rssfeed_item_title_length="50"; 
                    rssfeed_item_title_color="#666"; 
                    rssfeed_item_bgcolor="#fff"; 
                    rssfeed_item_bgimage=""; 
                    rssfeed_item_border_bottom="on"; 
                    rssfeed_item_source_icon="off"; 
                    rssfeed_item_date="off"; 
                    rssfeed_item_description="on"; 
                    rssfeed_item_description_length="120"; 
                    rssfeed_item_description_color="#666"; 
                    rssfeed_item_description_link_color="#333"; 
                    rssfeed_item_description_tag="off"; 
                    rssfeed_no_items="0"; 
                    rssfeed_cache = "e9d423c7130fec45e41aae68d9932e53"; 
                    //--> 
                    </script> 
                    <script type="text/javascript" src="//feed.surfing-waves.com/js/rss-feed.js"></script> 

            </div>
        </div>
                


                
            </div>

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>