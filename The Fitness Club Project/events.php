<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Events
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="images/events.png" alt="" style="border-top-left-radius: 7px; border-top-right-radius: 7px;">
            </div>
        </div>
        
        
        <div class="row" style="margin-top: 20px">
            <div class="col-md-6">
                
                <?php
                $query = "SELECT image FROM tbl_events WHERE eventId = '1';";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));    
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo '<img class="img-responsive" alt="" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                            }
                ?>
            </div>
            <div class="col-md-6">
                <h3 class="defaultfontbold" style="margin-top: 30px;">
                    <?php 
                        $query = "SELECT name FROM tbl_events
                                  WHERE eventId = '1'";
        
                        $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                    
                        while($row = mysqli_fetch_array($result))
                        {    
                        echo $row['name'];
                        }
                    
                    ?>
                </h3>
                <p class="defaultfont"><?php 
                        $query = "SELECT description FROM tbl_events
                                  WHERE eventId = '1'";
        
                        $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                    
                        while($row = mysqli_fetch_array($result))
                        {    
                        echo $row['description'];
                        }
                    
                    ?></p>
                <p class="defaultfontbold"><?php 
                        $query = "SELECT date, time, price FROM tbl_events
                                  WHERE eventId = '1'";
        
                        $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                    
                        while($row = mysqli_fetch_array($result))
                        {    
                        echo 'Date: '.$row['date'].'<br>';
                        echo 'Time: '.$row['time'].'<br>';
                        echo 'Price: €'.$row['price'];
                        }
                    
                    ?></p>
            </div>
        </div>
           
           <div class="row" style="margin-top: 10px"> 
            <div class="col-md-6">
                <h3 class="defaultfontbold" style="margin-top: 30px;"><?php           
                        $query = "SELECT name FROM tbl_events
                                  WHERE eventId = '2'";
        
                        $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                    
                        while($row = mysqli_fetch_array($result))
                        {    
                        echo $row['name'];
                        }
                    
                    ?></h3>
                <p class="defaultfont"><?php  
                        $query = "SELECT description FROM tbl_events
                                  WHERE eventId = '2'";
        
                        $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                    
                        while($row = mysqli_fetch_array($result))
                        {    
                        echo $row['description'];
                        }
                    
                    ?></p>
                <p class="defaultfontbold"><?php 
                        $query = "SELECT date, time, price FROM tbl_events
                                  WHERE eventId = '2'";
        
                        $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                    
                        while($row = mysqli_fetch_array($result))
                        {    
                        echo 'Date: '.$row['date'].'<br>';
                        echo 'Time: '.$row['time'].'<br>';
                        echo 'Price: €'.$row['price'];
                        }
                    
                    ?></p>
            </div>
            <div class="col-md-6">
                <?php
                $query = "SELECT image FROM tbl_events WHERE eventId = '2';";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));    
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo '<img class="img-responsive" alt="" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                            }
                ?>
            </div>
            <div class="col-md-12" style="margin-top: 30px;">
                <p class="defaultfont">All events are hosted at our gym <b><a href="location.php" style="text-decoration: none; color:#000000">located</a></b> in Birkirkara, Feel free to <b><a href="contact.php" style="text-decoration: none; color:#000000">contact us </a></b> for more information!</p>
                <p class="defaultfont"></p>   
            </div>
            
               
        </div>
        
        
    </div>

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    

</body>
</html>