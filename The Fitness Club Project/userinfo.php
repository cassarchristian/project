<?php 

session_start();

?>


<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
    
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Membership Info
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="images/membershipinfo.png" alt="" style="border-top-left-radius: 7px; border-top-right-radius: 7px;">
            </div>
        </div>
        
        <div class="col-lg-12" style="margin-top: 10px;">
                <ul id="myTab" class="nav nav-tabs nav-justified">
                    <li class="active defaultfontbold"><a href="#service-one" data-toggle="tab">My Membership</a>
                    </li>
                    <li class="defaultfontbold"><a href="#service-two" data-toggle="tab">Change Password</a>
                    </li>
                </ul>

                <div id="myTabContent" class="tab-content defaultfont">
                    <div class="tab-pane fade active in" id="service-one">
                        <h5 class="defaultfontbold" style="margin-top: 30px;text-align: center;letter-spacing: 0.6px;">
                    
                </h5>
                        
                        
                        
                        
                        
                       <div class="card">

                          <?php
                            
                                $username = $_SESSION['valid_user'];
                                $query = "SELECT image FROM tbl_member WHERE username = '$username';";
                                        $result = mysqli_query($conn, $query)
                                        or die("Error in query: ". mysqli_error($conn));    
                                        while($row = mysqli_fetch_array($result))
                                        {    
                                        echo '<img class="img-responsive img-circle" alt="" style="margin: 0 auto; width: 35%;padding: 2px;" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                                        }


                            ?>
                           <form id="contact-form" method="post" action="userinfo.php" role="form" enctype="multipart/form-data">
                               
                               <input class="form-control" type="file" name="profileimage" id="fileToUpload" accept="image/*" style="width: 230px;margin: 0 auto;margin-top: 10px;">
                               <input type="submit" name="changepic" class="changepic btn-send" value="Change Profile Picture">
                           
                           
                           </form>
                           
                          <h1><b><?php
                              $query = "SELECT name,surname FROM tbl_member WHERE username = '$username';";
                              
                              $result = mysqli_query($conn, $query)
                                            or die("Error in query: ". mysqli_error($conn));
                              
                              while($row = mysqli_fetch_array($result))
                                {   
                                
                                    echo $row['name']. " " . $row['surname'];
                                }
                              
                              
                              
                              
                              ?></b></h1>
                           
                           
                           
                          <p class="title"><?php
                              $query = "SELECT username FROM tbl_member WHERE username = '$username';";
                              
                              $result = mysqli_query($conn, $query)
                                            or die("Error in query: ". mysqli_error($conn));
                              
                              while($row = mysqli_fetch_array($result))
                                {   
                                
                                    echo $row['username'];
                                }
                              
                              
                              
                              
                              ?></p>
                          <p>The Fitness Club Member</p>
                          <p><?php
                              
                              $query = "SELECT emailAddress, dateRegistered FROM tbl_member WHERE username = '$username';";
                              
                              $result = mysqli_query($conn, $query)
                                            or die("Error in query: ". mysqli_error($conn));
                              
                              while($row = mysqli_fetch_array($result))
                                {   
                                
                                    echo "<b>Date Registered - </b>".$row['dateRegistered']."<br>";
                                  echo "<b>Email - </b>".$row['emailAddress'];
                                }
                              
                              
                              ?></p>
                         <p><button class="userinfobutton"><?php 
                             
                             $query = "SELECT typeName FROM tbl_membershiptype INNER JOIN tbl_member ON tbl_member.membershipTypeId = tbl_membershiptype.membershipTypeId WHERE username = '$username';"; 
                             
                             $result = mysqli_query($conn, $query)
                                            or die("Error in query: ". mysqli_error($conn));
                              
                              while($row = mysqli_fetch_array($result))
                                {   
                                
                                    echo $row['typeName'];
                                }
                             
                             
                             ?></button></p>
                           
                           
                            
                           
                           
                           
                        </div> 
                        
                        
                        
                        
                        
                        
                        
                        
                    </div>
                    <div class="tab-pane fade" id="service-two">
                        <form id="contact-form" method="post" action="userinfo.php" role="form">

                <div class="controls defaultfont" style="margin-top: 20px; text-align: center;">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_name">Current Password *</label>
                                <input id="form_name" type="password" name="currentpassword" class="form-control" placeholder="Please enter your current password *" required="required" data-error="Current Password is required.">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_lastname">Updated Password *</label>
                                <input id="form_lastname" type="password" name="updatedpassword" class="form-control" placeholder="Please enter your updated password *" required="required" data-error="Updated password is required.">
                                
                            </div>
                        </div>
                        
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_phone">Confirm Updated Password *</label>
                                <input id="form_phone" type="password" name="conupdatedpassword" class="form-control" placeholder="Please enter your updated password again *"
                                required="required" data-error="Updated password is required.">
                                
                            </div>
                        </div>
                    
                        <div class="col-md-6" style="margin-top: 25px;">
                            <div class="form-group">
                                <input type="submit" name="submit" class="form-control btn defaultbutton btn-send" value="Change Password">
                            </div>
                        </div>
                    
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
        
    <?php
    
        if(isset($_POST['submit'])){
                        $currentPass = $_POST['currentpassword'];
                        $updatedPassword = $_POST['updatedpassword'];
                        $conupdatedPassword = $_POST['conupdatedpassword'];
                        $username = $_SESSION['valid_user'];
                        $password = $_SESSION['valid_pass'];
                        
            
            if(sha1($currentPass) != $password){
                echo "<div class='container'>
                        <div class='row'>
                        <h3 class='defaultfontbold'>Error: Password could not be changed.</h3>
                        <p class='defaultfontbold' style='color: red'>Current Password is invalid, please try again!<p>
                        </div>
                      </div>";
                
            }
            else if(sha1($updatedPassword) == $password){
                echo "<div class='container'>
                        <div class='row'>
                        <h3 class='defaultfontbold'>Error: Password could not be changed.</h3>
                        <p class='defaultfontbold' style='color: red'>Updated Password is the same as your current password!<p>
                        </div>
                      </div>";
            }
            else if(strlen($updatedPassword) < 7){
                echo "<div class='container'>
                        <div class='row'>
                        <h3 class='defaultfontbold'>Error: Password could not be changed.</h3>
                        <p class='defaultfontbold' style='color: red'>Updated Password should be more than 6 characters long!<p>
                        </div>
                      </div>";
            }
            else if($updatedPassword != $conupdatedPassword){
                echo "<div class='container'>
                        <div class='row'>
                        <h3 class='defaultfontbold'>Error: Password could not be changed.</h3>
                        <p class='defaultfontbold' style='color: red'>Updated Password did not match, please try again!<p>
                        </div>
                      </div>";
            }else{
                $conn = mysqli_connect("localhost", "root", "", "thefitnessclub_db");
                        if (mysqli_connect_errno()){
                            echo "Error: Could not connect to database. Please try again
                            later";
                            exit; 
                        }
                
                        $newencrypted = sha1($updatedPassword);
                
                        $query = "UPDATE tbl_member
                                  SET password = '$newencrypted'
                                  WHERE username = '$username'";
        
                        $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                        sleep(2);
                        session_destroy();
                        echo "<meta http-equiv='refresh' content=\"0; url=passchanged.php\">";
                        exit();

            }
            
                        
        }
        
        if(isset($_POST['changepic'])){
                        $limitsize=4294967295;
                        
                        if($_FILES['profileimage']['size'] != 0){
                            $image = addslashes(file_get_contents($_FILES['profileimage']['tmp_name'])); //SQL Injection defence!
                            $imagesize=$_FILES['profileimage']['size'];
                            
                            $username = $_SESSION['valid_user'];
                            
                            $conn = mysqli_connect("localhost", "root", "", "thefitnessclub_db");
                            if (mysqli_connect_errno()){
                                echo "Error: Could not connect to database. Please try again
                                later";
                                exit; 
                            }

                            $query = "UPDATE tbl_member
                                      SET image = '{$image}'
                                      WHERE username = '$username'";

                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='far fa-check-square' style='margin-right: 10px;'></i>Profile Image has been updated.</h3>
                                    <p class='defaultfontbold'>Please refresh the page to see the result!<p>
                                    </div>
                                  </div>";
                            
                            
                            
                        }else{
                            echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Profile Image can not be changed/updated.</h3>
                                    <p class='defaultfontbold' style='color: red'>Please choose a file image!<p>
                                    </div>
                                  </div>";
                        }
            
            
            
        }
    
        
    
    
    
    
    
    
    
    
    
    ?>
        
        
    

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>