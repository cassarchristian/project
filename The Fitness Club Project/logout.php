<?php
    session_start();
    unset($_SESSION['valid_user']);
    unset($_SESSION['valid_pass']);
    session_destroy();
    sleep(2);
    header('Location: index.php');

?>