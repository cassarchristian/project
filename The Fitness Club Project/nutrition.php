<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Home Workouts
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="images/nutritioninfo.png" alt=""  style="border-top-left-radius: 7px; border-top-right-radius: 7px;">
            </div>
        </div>
        
        <div class="row" style="margin-top: 30px">
            <div class="col-md-12">
                <p class="defaultfont">Nutrition is the science that interprets the interaction of nutrients and other substances in food in relation to maintenance, growth, reproduction, health and disease of an organism. We offer free diet plans to increase your metabolism as well as help you reach your goals!</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <h3 class="defaultfontbold text-center" style="margin-top: 30px;">Fat-loss diet plan</h3>
                <p class="defaultfont text-center" style="margin-left: 70px; margin-right: 70px;">Weight loss, in the context of medicine, health, or physical fitness, refers to a reduction of the total body mass, due to a mean loss of fluid, body fat or adipose tissue or lean mass, namely bone mineral deposits, muscle, tendon, and other connective tissue.</p>
                <img class="img-responsive center-block" src="images/fatlossdiet.jpg">
            </div>
        </div>
        
        
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <h3 class="defaultfontbold text-center" style="margin-top: 30px;">Muscle-building diet plan</h3>
                <p class="defaultfont text-center" style="margin-left: 70px; margin-right: 70px;">Muscle hypertrophy involves an increase in size of skeletal muscle through a growth in size of its component cells. Two factors contribute to hypertrophy: sarcoplasmic hypertrophy, which focuses more on increased muscle glycogen storage; and myofibrillar hypertrophy, which focuses more on increased myofibril size.</p>
                <img class="img-responsive center-block" src="images/musclebuildingdiet.png">
            </div>
        </div>
        
        

            <div class="col-md-12" style="margin-top: 30px;">
                
                <p class="defaultfont text-center">Feel free to contact our <b><a href=trainers.php style="text-decoration: none; color:#000000">nutrionist </a></b> to assist you!</p>
            </div>
        </div>
 

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>