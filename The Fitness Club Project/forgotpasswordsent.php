<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Forgot Password
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row" style="height: 300px;">
            <div class="col-lg-12 defaultfont">
                <h2><b>Your password request has been sent!</b></h2>
                <p><b>We will send you an email with further details.</b></p>
                    <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-square" aria-hidden="true" style="color: #FF1010; margin-right: 10px; font-size: 24px;"></i></a>
                    <a href="https://www.twitter.com" target="_blank"><i class="fab fa-twitter-square" aria-hidden="true" style="color: #FF1010; margin-right: 10px; font-size: 24px;"></i></a>
                    <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram" aria-hidden="true" style="color: #FF1010;margin-right: 10px; font-size: 24px;"></i></a>
            </div>
        </div>
        
        
    </div>

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    

    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>