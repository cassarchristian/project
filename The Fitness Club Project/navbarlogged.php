     <?php
        require_once("conn.php");
        $conn = connectDB();              
    ?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="box-shadow: 0px 0px 47px 6px rgba(0,0,0,0.55);">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.php" class="navbar-left"><img id="logo" src="images/logo.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="navbarlink" href="about.php">ABOUT US</a>
                        </li>
                        <li>
                            <a class="navbarlink" href="location.php">LOCATION</a>
                        </li>
                        <li>
                            <a class="navbarlink" href="trainers.php">TEAM</a>
                        </li>
                        <li>
                            <a class="navbarlink" href="contact.php">CONTACT</a>
                        </li>
                        
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle navbarlink" data-toggle="dropdown">MY ACCOUNT<span class="caret"></span></a>
                            <ul id="login-dp" class="dropdown-menu">
                                <li>
                                     <div class="row">
                                            <div class="col-md-12">
                                                <div class="profile-header-container">   
                                                        <div class="profile-header-img">   
                                                            <?php
                                                                $username = $_SESSION['valid_user'];
                                                                $password = $_SESSION['valid_pass'];
    

                                                                $imgquery = "SELECT image FROM tbl_member WHERE username = '$username';";
                                                                $imgresult = mysqli_query($conn, $imgquery)
                                                                or die("Error in query: ". mysqli_error($conn));    
                                                                while($row = mysqli_fetch_array($imgresult))
                                                                {    
                                                                echo '<img class="img-circle" alt=""  src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                                                                }
                                                            ?>       
                                                            <!-- badge -->
                                                            <div class="rank-label-container">
                                                                <?php
                                                
                                                    $username = $_SESSION['valid_user'];
                                                    $password = $_SESSION['valid_pass'];
                                                    
                                                    
   
                                                    
                                                                    
                                                    $query = "SELECT * FROM tbl_member WHERE username = '$username';";
                                                    $result = mysqli_query($conn, $query)
                                                    or die("Error in query: ". mysqli_error($conn));

                                                    while($row = mysqli_fetch_array($result))
                                                    {    
                                                    echo $row['username'];
                                                    echo "<br>";
                                                    echo $row['name']." ".$row['surname'];

                                                    } 
                                                    ?>       
                                                            </div>
                                                        </div>
                                                    </div> 
                                                
                                                    <div class="form-group">
                                                        <a href="userinfo.php" style="text-decoration:none;"><button class="btn btn-block defaultbutton">Membership Info</button></a>
                                                    </div>
                                                
                                                     <div class="form-group">
                                                        <a href="events.php" style="text-decoration:none;"><button class="btn btn-block defaultbutton">Events</button></a>
                                                    </div>
                                                
                                                    <div class="form-group">
                                                        <a href="videos.php" style="text-decoration:none;"><button class="btn btn-block defaultbutton">Workout Videos</button></a>
                                                    </div>
                                                    <div class="form-group">
                                                        <a href="fitnessnews.php" style="text-decoration:none;"><button class="btn btn-block defaultbutton">Fitness News</button></a>
                                                    </div>
                                                    <div class="form-group">
                                                        <a href="nutrition.php" style="text-decoration:none;"><button class="btn btn-block defaultbutton">Nutritional Plans</button></a>
                                                    </div>
                                                          
                                                    <div class="form-group">
                                                        <a href="reportfault.php" style="text-decoration:none;"><button class="btn btn-block defaultbutton">Report Faulty Equipment</button></a>
                                                    </div>
                                                
                                                 <form class="form defaultfont" role="form" method="post" action="logout.php" accept-charset="UTF-8" id="logout-nav">
                                                        <div class="form-group">
                                                             <button type="submit" class="btn btn-block defaultbuttondark">Sign Out</button>
                                                        </div>
                                                 </form>
                                            </div>
                                            <div class="bottom text-center defaultfont">
                                                <b>Have any feedback or suggestions? <a href="contact.php" style="text-decoration: none; color: black;">Click here!</a></b>
                                            </div>
                                     </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
