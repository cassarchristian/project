<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Forgot Password
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>

        
        
        <div class="row">
            <div class="col-md-12">
                <p class="defaultfont">Please enter your email in the field below. We will be sending you an email with a generated password in which you can change in your membership info section upon login!</p>
                    <form method="post" action="forgotpassword.php" role="form">
                        <div class="col-md-6 defaultfont">
                            <div class="form-group">
                                <label>Email *</label>
                                <input id="form_phone" type="email" name="email" class="form-control" placeholder="Please enter your email *"
                                required="required" data-error="Email is required.">
                            </div>
                        </div>

                        <div class="col-md-2 defaultfont" style="margin-top: 25px;">
                            <div class="form-group">
                                <input type="submit" name="submit" class="form-control btn defaultbutton btn-send" value="Submit">
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    
        </div>
    
    
        <?php
        if(isset($_POST['submit'])){
            $email = mysqli_real_escape_string($conn,trim($_POST['email']));
            
                            
                $query = "select count(*) from tbl_member where emailAddress = '$email'"; 
                
                $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                
                $row = mysqli_fetch_row($result);
                
                $count = $row[0];
                    if($count > 0){
                        
                        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $charactersLength = strlen($characters);
                        $randomString = '';
                        $length = 10;
                        for ($i = 0; $i < $length; $i++) {
                            $randomString .= $characters[rand(0, $charactersLength - 1)];
                        }
                        
                        
                        $encryptedrandomString = sha1($randomString);
                        
                        $query = "UPDATE tbl_member
                                  SET password = '$encryptedrandomString'
                                  WHERE emailAddress = '$email'";
        
                        $result = mysqli_query($conn, $query)
                        or die("Error in query: ". mysqli_error($conn));
                        
                        
                        $to = $email;
                        $subject = 'The Fitness Club - Forgot Password';
                        $message = 'You have requested to reset your password as it was forgotten, please use the password below to login again. Note that this password can be changed by signing in and going to your membership info section.' . "\n\n" . "New Password: " . $randomString;
                        $headers = 'From: thefitnesscluborg@gmail.com' . "\r\n" .
                                        'Reply-To:'. $email . "\r\n";
                        mail($to, $subject, $message, $headers);
                        
                        
                        
                        sleep(3);
                        echo "<meta http-equiv='refresh' content=\"0; url=forgotpasswordsent.php\">";
                        exit();
                        
                        
                    }else{
                       echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Request could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>The email address you entered is not assosciated with any membership account!<p>
                                    </div>
                                  </div>";
                    } 
        
            
        }
    
    ?>
 

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>