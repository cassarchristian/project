<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Pricing
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="images/jointoday.png" style="border-top-left-radius: 7px; border-top-right-radius: 7px;" alt="">
            </div>
        </div>
        
        <div class="row">
        <div id="generic_price_table">   
                <div class="col-md-6">
                    <div class="generic_content clearfix">
                        <div class="generic_head_price clearfix">
                            <div class="generic_head_content clearfix">
                                <div class="head_bg"></div>
                                <div class="head">
                                    <span>BEGINNER PLAN</span>
                                </div>
                            </div>
                            <div class="generic_price_tag clearfix">	
                                <span class="price">
                                    <span class="sign">€</span>
                                    <span class="currency">50</span>
                                    <span class="cent">.00</span>
                                </span>
                            </div>
                        </div>                            
                        <div class="generic_feature_list">
                        	<ul>
                            	<li><span>1 Month</span> Access to the Gym</li>
                                <li><span>Open</span> Monday to Sunday</li>
                            </ul>
                        </div>
                        <div class="generic_price_btn clearfix">
                        	<a class="" href="application.php" target="_blank">Apply</a>
                        </div>
                    </div>      
                </div>
                
                <div class="col-md-6">
                    <div class="generic_content clearfix">
                        <div class="generic_head_price clearfix">
                            <div class="generic_head_content clearfix">
                                <div class="head_bg"></div>
                                <div class="head">
                                    <span>INTERMEDIATE PLAN</span>
                                </div>
                            </div>
                            <div class="generic_price_tag clearfix">	
                                <span class="price">
                                    <span class="sign">€</span>
                                    <span class="currency">120</span>
                                    <span class="cent">.00</span>
                                </span>
                            </div>     
                        </div>                            
                        <div class="generic_feature_list">
                        	<ul>
                            	<li><span>3 Month</span> Access to the Gym</li>
                                <li><span>Open</span> Monday to Sunday</li>
                                <li><span>5%</span> Discount on Succeeding Membership</li>
                            </ul>
                        </div>
                        <div class="generic_price_btn clearfix">
                        	<a class="" href="application.php" target="_blank">Apply</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="generic_content clearfix">
                        <div class="generic_head_price clearfix">
                            <div class="generic_head_content clearfix">
                                <div class="head_bg"></div>
                                <div class="head">
                                    <span>ADVANCED PLAN</span>
                                </div>
                            </div>
                            <div class="generic_price_tag clearfix">	
                                <span class="price">
                                    <span class="sign">€</span>
                                    <span class="currency">180</span>
                                    <span class="cent">.00</span>
                                </span>
                            </div>
                        </div>                            
                        <div class="generic_feature_list">
                            <ul>
                            	<li><span>6 Month</span> Access to the Gym</li>
                                <li><span>Open</span> Monday to Sunday</li>
                                <li><span>10%</span> Discount on Succeeding Membership</li>
                                <li><span>Free</span> 'The Fitness Club' Towel</li>
                            </ul>
                        </div>
                            <div class="generic_price_btn clearfix">
                        	   <a class="" href="application.php" target="_blank">Apply</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    <div class="generic_content clearfix">
                        <div class="generic_head_price clearfix">
                            <div class="generic_head_content clearfix">
                                <div class="head_bg"></div>
                                <div class="head">
                                    <span>FIT-LIFE PLAN</span>
                                </div>      
                            </div>
                            <div class="generic_price_tag clearfix">	
                                <span class="price">
                                    <span class="sign">€</span>
                                    <span class="currency">260</span>
                                    <span class="cent">.00</span>
                                </span>
                            </div>
                        </div>                            
                        <div class="generic_feature_list">
                        	<ul>
   	                            <li><span>12 Month</span> Access to the Gym</li>
                                <li><span>Open</span> Monday to Sunday</li>
                                <li><span>20%</span> Discount on Succeeding Membership</li>
                                <li><span>€10</span> Eurosport Voucher</li>
                                <li><span>Free</span> 'The Fitness Club' Towel</li>
                            </ul>
                        </div>
                            <div class="generic_price_btn clearfix">
                        	   <a class="" href="application.php" target="_blank">Apply</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row" style="margin-top: 18px; margin-bottom: 18px;">
        <div class="col-md-12">
            <h2 class="defaultfontbold">Terms and Agreements</h2>
            <p class="defaultfont">Please refer to the gym guidelines, terms and regulations by clicking<span><a href="location.php" style="text-decoration: none; color:#000000"><b> here</b></a></span>.</p>
            <p class="defaultfont">If you have any queries or would like to learn more about prices and offers such as family packs and student plans, please<span><a href="contact.php" style="text-decoration: none; color:#000000"><b> contact us</b></a></span>.</p>
        </div>
    </div>
        
    </div>  

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    
     
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>