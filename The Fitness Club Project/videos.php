<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Home Workouts
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="images/homeworkoutimg.png" alt=""  style="border-top-left-radius: 7px; border-top-right-radius: 7px;">
            </div>
        </div>
        
        <div class="row" style="margin-top: 30px">
            <div class="col-md-12">
                <p class="defaultfont">Couldnt make it to the gym? Take a look at these videos below from our sponsored partners and athletes. Each video section guides you about different workouts 
                which you can do at easily at home!</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <h3 class="defaultfontbold" style="margin-top: 30px;">General Workouts</h3>
                <p class="defaultfont" style="margin-bottom: 30px;">A workout/physical exercise is any bodily activity that enhances or maintains physical fitness and overall health and wellness.[1] It is performed for various reasons, including increasing growth and development, preventing aging, strengthening muscles and the cardiovascular system, honing athletic skills, weight loss or maintenance, and also enjoyment.</p>
            </div>
            <div class="embed-responsive embed-responsive-16by9 sizer">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YdB1HMCldJY"></iframe>
            </div>
            <br>
            <div class="embed-responsive embed-responsive-16by9 sizer">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/fcN37TxBE_s"></iframe>
            </div> 
            
            <br>
            <div class="col-md-12">
                <h3 class="defaultfontbold" style="margin-top: 30px;">Muscle Building</h3>
                <p class="defaultfont" style="margin-bottom: 30px;">Muscle building/hypertrophy involves an increase in size of skeletal muscle through a growth in size of its component cells. Two factors contribute to hypertrophy: sarcoplasmic hypertrophy, which focuses more on increased muscle glycogen storage; and myofibrillar hypertrophy, which focuses more on increased myofibril size.</p>
            </div>
            <div class="embed-responsive embed-responsive-16by9 sizer">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ISYkFJiWfhI"></iframe>
            </div>
            <br>
            <div class="embed-responsive embed-responsive-16by9 sizer">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/xCRUFI0XcB0"></iframe>
            </div>
            <div class="col-md-12" style="margin-top: 30px;">
                <p class="defaultfont">Feel free to contact any of our <b><a href=trainers.php style="text-decoration: none; color:#000000">trainers</a></b> for further help and guidance!</p>
            </div>
        </div>
    </div>

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>