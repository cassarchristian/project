<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Report a Faulty Equipment
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        

        
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header defaultfontbold">Please fill in the form below
                    <br><small class="defaultfont">Encountered a faulty equipment or any defect in our gym?</small>
                </h4>
            </div>
            
            <div class="col-md-10">
                <form id="contact-form" method="post" action="reportfault.php" role="form">

                <div class="messages"></div>

                <div class="controls defaultfont">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_name">First Name *</label>
                                <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your name *" required="required" data-error="Firstname is required.">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_lastname">Last Name *</label>
                                <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your surname *" required="required" data-error="Lastname is required.">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="sel1">Equipment *</label>
                              <select class="form-control" name="equipment" id="sel1">
                               <?php
                                            


                                            $query = "SELECT * FROM tbl_equipment ORDER BY equipmentId";
                                            
                                            $result = mysqli_query($conn, $query)
                                            or die("Error in query: ". mysqli_error($conn));
                                      
                                            while($row = mysqli_fetch_array($result))
                                            {    
                                            echo '<option value='.$row['equipmentId'].'>'.$row['name'].'</option>';
                                            }



                                        ?>
                                </select>
                            </div>
                        </div>
                        
                    </div>
                
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="form_message">Comments</label>
                                <textarea id="form_message" name="message" class="form-control" placeholder="Leave a comment if applicable" rows="4"  data-error="Please,leave us a message."></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" name="submit" class="btn defaultbutton btn-send" value="Report">
                        </div>
                    </div>
                    </div>
            </form>
            </div>
        </div>
    
        
    </div>
    <?php
    
    if(isset($_POST['submit'])){
                        $equipment = $_POST['equipment'];
                        $name = $_POST['name'];
                        $surname = $_POST['surname'];
                        $email = $_POST['email'];
                        $comment = $_POST['message'];  
                        
                        
        
        
                       if(is_numeric($name)){
                            echo "<div class='container'>
                                    <div class='row'>
                                    <h3 class='defaultfontbold'>Error: Message could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>First name is numeric, please try again!<p>
                                    </div>
                                  </div>";

                        }
                        else if(is_numeric($surname)){
                            echo "<div class='container'>
                                    <div class='row'>
                                    <h3 class='defaultfontbold'>Error: Message could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>Last name is numeric, please try again!<p>
                                    </div>
                                  </div>";

                        }else {
                            $query = "UPDATE tbl_equipment
                                  SET faultStatus = 1
                                  WHERE equipmentId = $equipment";
        
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));

                            $to = "thefitnesscluborg@gmail.com";
                            $subject = 'The Fitness Club - Faulty Equipment';
                            $txt = $name . " " .$surname . ' has sent a faulty equipment report.' . "\n\n" . "Equipment ID: " . $equipment . 
                            "\n\n" . 'Contact member on: ' . $email;
                            $headers = "From:" . $email;


                            if(mail($to, $subject, $txt, $headers)) { 
                                sleep(2);
                                echo "<meta http-equiv='refresh' content=\"0; url=reportsent.php\">";
                                exit(); 
                            } else { 
                              echo "There was a problem"; 
                            } 
                            
                        }

        
                         
        
                        

                }
    ?>
    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>