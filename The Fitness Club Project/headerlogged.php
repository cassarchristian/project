<header id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image: url(images/landing11.jpg)"></div>
                <div class="carousel-caption">
                    <h2 class="quotes">"Do not stop until you are done"</h2>
                    <a href="userinfo.php" class="landinglink"><b>MY MEMBERSHIP</b></a>
                </div>        
            </div>
            <div class="item">
                <div class="fill" style="background-image:url(images/landing22.jpg);"></div>
                <div class="carousel-caption">
                    <h2 class="quotes">"Stop wishing and start doing"</h2>
                    <a href="events.php" class="landinglink"><b>EVENTS</b></a>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url(images/landing33.jpg);"></div>
                <div class="carousel-caption">
                    <h2 class="quotes">"Slow progress is better than no progress"</h2>
                    <a href="nutrition.php" class="landinglink"><b>NUTRITION</b></a>
                </div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

