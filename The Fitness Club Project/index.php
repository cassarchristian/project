<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
    
    
    

</head>

<body>
    <!-- Navigation -->
    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'headerlogged.php';

            }else{

                include 'header.php';

            }
        ?>
    

    
    
        <div class="container">
            <div class="row text-center defaultfont" style="margin-top: 20px;">

            <div class="col-md-4 col-sm-4 hero-feature">
                <div class="thumbnail indexbox">
                    <img src="images/box1.jpg" alt="">
                    <div class="caption">
                        <h3>About Us</h3>
                        <p>We are highly recommonded by the islands most successful athletes.</p>
                        <p>
                            <a href="about.php" class="landinglink">Learn More</a>
                        </p>
                    </div>
                </div>
            </div>
                
            <div class="col-md-4 col-sm-4 hero-feature">
                <div class="thumbnail indexbox">
                    <img src="images/box2.jpg" alt="">
                    <div class="caption">
                        <?php
                            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
                            {
                                echo '<h3>Events</h3>';
                                echo '<p>Our club schedules different fitness events hosted by our team.</p>';
                                echo '<p>
                                        <a href="events.php" class="landinglink">More Info</a>
                                      </p>';

                            }else{

                                echo '<h3>Memberships</h3>';
                                echo '<p>Offering diverse membership types with reasonable prices.</p>';
                                echo '<p>
                                        <a href="prices.php" class="landinglink">Join Now</a>
                                      </p>';

                            }
                        ?>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-4 hero-feature">
                <div class="thumbnail indexbox">
                    <img src="images/box3.jpg" alt="">
                    <div class="caption">
                        <h3>Location</h3>
                        <p>We currently manage one successful club, fully equipped and located in Birkirkara.</p>
                        <p>
                            <a href="location.php" class="landinglink">View Location</a>
                        </p>
                    </div>
                </div>
            </div>


        </div>

    </div>
    
    <div class="container-fluid">
        <div class="row">
                <div class="jumbotron" style="border-top-left-radius: 10px; border-top-right-radius: 10px;margin-top: 4px;">
                    <div>
                        <h2><b>A POSITIVE CHANGE TO PEOPLE'S LIVES</b></h2>
                        <div class="jumbotronspacing">
                            <p>It is our believe that with some encouragement, setting of realistic goals, anyone coming to our clubs can feel better and have fun and minimise stress. It is our vision to provide clubs that help people get fit, feel great, in a friendly and welcoming atmosphere, helped by friendly staff, and using state of the art facilities.
                            </p>
                        </div>
                    </div>
                </div>
        </div>
    
    
    
    </div>
    
    
    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 
    })
        
    $(document ).ready(function() {
        $('#myCarousel').hide().fadeIn(1000);
        
    });
        
    $(window).scroll(function () {
        console.log($(window).scrollTop());
        var topDivHeight = $(".topdiv").height();
        var viewPortSize = $(window).height();

        var triggerAt = 150;
        var triggerHeight = (topDivHeight - viewPortSize) + triggerAt;

        if ($(window).scrollTop() >= triggerHeight) {
            $('.thumbnail').css('visibility', 'visible').hide().fadeIn(1500);
            $(this).off('scroll');
            $('.jumbotron').css('visibility', 'visible').hide().fadeIn(2500);
            $(this).off('scroll');
        }
    });
    
    </script>
    
    
    
</body>
</html>
