<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
   
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Location
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="images/location.png" style="border-top-left-radius: 7px; border-top-right-radius: 7px;" alt="">
            </div>
        </div>

        <div class="row" style="margin-top: 20px;">
            <!-- Map Column -->
            <div class="col-md-8">
                <!-- Embedded Google Map -->
                <div id="map">
                    <!--MAP HERE-->
                </div>
                
            </div>
            <!-- Contact Details Column -->
            <div class="col-md-4">
                <h3 class="defaultfontbold">The Fitness Club Centre</h3>
                <p class="defaultfont">
                    The Fitness Club, <br>Fleur De Lys, Birkirkara, BKR1480<br>
                </p>
                
                <p class="defaultfont">
                    The Fitness Club Centre is our main campus, it was built in 2010 to provide our clients the best
                    fitness experience. Our manager and personal trainers are found there anytime in the opening hours.
                </p>
                <p class="defaultfont"><b><a href=gallery.php style="text-decoration: none; color:#000000">View Gallery</a></b></p>
                <br>
                <p class="defaultfont" ><i class="fa fa-phone"></i> 
                    +365 79123456</p>
            </div>
            
            <!-- Service Tabs -->
        
            <div class="col-lg-12" style="margin-top: 10px;">

                <ul id="myTab" class="nav nav-tabs nav-justified">
                    <li class="active defaultfontbold"><a href="#service-one" data-toggle="tab"> Opening Hours</a>
                    </li>
                    <li class="defaultfontbold"><a href="#service-two" data-toggle="tab"> Terms and Regulations</a>
                    </li>
                </ul>

                <div id="myTabContent" class="tab-content defaultfont">
                    <div class="tab-pane fade active in" id="service-one">
                        <table>
                            <br>
                            <tr><th class="tablepad">Monday</th><td class="positioning">05:00 - 23:00</td></tr>
                            <tr><th class="tablepad">Tuesday</th><td class="positioning">05:00 - 23:00</td></tr>
                            <tr><th class="tablepad">Wednesday </th><td class="positioning">06:00 - 22:00</td></tr>
                            <tr><th class="tablepad">Thursday</th><td class="positioning">05:00 - 23:00</td></tr>
                            <tr><th class="tablepad">Friday</th><td class="positioning">05:00 - 23:00</td></tr>
                            <tr><th class="tablepad">Saturday</th><td class="positioning">07:00 - 20:00</td></tr>
                            <tr><th class="tablepad">Sunday</th><td class="positioning">Closed</td></tr>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="service-two">
                            <p style="margin-top: 15px;">
                            <b>Please refer to the regulations below:</b><br><br>
                            1. Show membership card upon gym access<br><br>
                            2. To train in the gym you need to bring proper sports clothing and clean sports shoes. Training in jeans, wearing boots, heels, flip flops, barefoot or on socks is not allowed. Taking of shoes is only allowed on the red mats for streching and foam rolling.<br><br>
                            3. Bring and use a towel for hygiene and to keep yourself and the equipment dry. If you forgot to bring a towel, you can (and will be asked to) rent one at the front desk. The collected money will be donated/used for charity.<br><br>
                            4. Stay hydrated during your training and fill your bottles at our water tap in the gym. Usage of cups or non-sealable drinks is not allowed.<br><br>
                            5. We have designated areas for clothing and shoe changes. Please do not use the gym as your personal changing area.<br><br>
                            6. In a gym, equipment is considered communal property. So please be friendly to other members and share equipment when it's crowded or when requested. You can easily alternate with another person while training and resting using either machines or free weights.<br><br>
                            7. Please take into account that you are not the only one in the gym and your behaviour can influence other members. Aggressive or annoying behaviour such as screaming, yelling, roaring is not allowed and will be addressed.
                            </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-12">
                <a target="_blank" href="https://www.eurosport.com.mt/"><img class="img-responsive" src="images/eurosport.gif" style="border-top-left-radius: 7px; border-top-right-radius: 7px;" alt=""></a>
            </div>
        </div>
        
        
        <div class="row" style="margin-top: 20px;">
            <!-- Map Column -->
            <div class="col-md-8">
                <!-- Embedded Google Map -->
                <div id="map2">
                    <!--MAP HERE-->
                </div>
            </div>
            <!-- Contact Details Column -->
            <div class="col-md-4">
                <h3 class="defaultfontbold">Eurosport Malta</h3>
                <p class="defaultfont">
                    Eurosport Malta, <br>Salvu Psaila Road, Birkirkara, BKR1550<br>
                </p>
                <p class="defaultfont">
                    Eurosport is our main sponsor partner - built on the values of Integrity, Value, Sportsmanship, Passion and Discipline - values, which echo throughout our team members. The brand is a power statement, a belief that winners are not those who cross the line first, but those who cross the finish line because they never give up.
                </p>
                <p class="defaultfont"><b><a href="https://www.eurosport.com.mt/" target="_blank" style="text-decoration: none; color:#000000">Visit Website</a></b></p>
                <br>
                <p class="defaultfont" ><i class="fa fa-phone"></i> 
                    +365 23859259</p>
            </div>
        </div>
    </div>

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    
     <script>
      function initMap()
    {
        var uluru = {lat: 35.8939921, lng: 14.469401};
        
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
        
        
        
        var uluru2 = {lat: 35.8928701, lng: 14.458335};
        
        var map2 = new google.maps.Map(document.getElementById('map2'), {
          zoom: 15,
          center: uluru2
        });
        
        var marker2 = new google.maps.Marker({
          position: uluru2,
          map: map2
        });
    }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM-TqwW1mVL4JJiSVFUYXPpjpKLSF_Br0&callback=initMap"
        type="text/javascript">
    </script>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>