<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">The Team
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="images/teamheader.png" style="border-top-left-radius: 7px; border-top-right-radius: 7px;" alt="">
            </div>
        </div>
     
        <div class="row" style="margin-top: 18px; margin-bottom: 18px;">
            <div class="col-md-12">
                <h2 class="defaultfontbold">Experience and Contribution</h2>
                <p class="defaultfont">Our Team make sures that you are safe while working out at all times. With experience, our personal trainers and the nutrionist are there to guide you gain your goals. Please login to be able to contact any member of the team via email.</p>
            </div>
        </div>
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <?php
                        $query = "SELECT image FROM tbl_staffmember WHERE staffMemberId = 1;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));    
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo '<img class="img-responsive" alt="" style="border-top-right-radius: 5px;border-top-left-radius: 5px" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                            }
                    ?>

                    <div class="caption">
                        <h3 class="defaultfontbold">
                            <?php 
                            $query = "SELECT * FROM tbl_staffmember WHERE staffMemberId = 1;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['name']." ".$row['surname'];

                            } ?>
                            
                            <br>
                            <small>
                                
                            <?php                          
                            $query = "SELECT staffOccupationName FROM tbl_staffoccupation INNER JOIN tbl_staffmember ON tbl_staffmember.staffOccupationId = tbl_staffoccupation.staffOccupationId WHERE staffMemberId = 1";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['staffOccupationName'];

                            }?> 
                                
                            </small>
                        </h3>
                        
                        <ul class="list-inline">
                            <?php
                                if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
                                {   
                                    echo "<li><a href='mailto:robertgaleatfc@gmail.com'><i class='fas fa-envelope emailbutton'></i></a><br>
                                        </li><br>";
                                    $query = "SELECT emailAddress, contactNumber FROM tbl_staffmember WHERE staffMemberId = 1";
                                    $result = mysqli_query($conn, $query)
                                    or die("Error in query: ". mysqli_error($conn));

                                    while($row = mysqli_fetch_array($result))
                                    {    
                                    echo "<small class='defaultfontbold'>".$row['emailAddress'] . "<br>" . "+365 " .$row['contactNumber']."</small>";

                                    }
                                    

                                }
                            ?>

                        </ul>
                        
                        
                        
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <?php
                        $query = "SELECT image FROM tbl_staffmember WHERE staffMemberId = '2';";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));    
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo '<img class="img-responsive" alt="" style="border-top-right-radius: 5px;border-top-left-radius: 5px" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                            }
                    ?>
                    <div class="caption">
                        <h3 class="defaultfontbold">
                            <?php 
                            
                            $query = "SELECT * FROM tbl_staffmember WHERE staffMemberId = 2;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['name']." ".$row['surname'];

                            } ?>
                            
                            <br>
                            <small>
                                
                            <?php                          
                            $query = "SELECT staffOccupationName FROM tbl_staffoccupation INNER JOIN tbl_staffmember ON tbl_staffmember.staffOccupationId = tbl_staffoccupation.staffOccupationId WHERE staffMemberId = 2";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['staffOccupationName'];

                            }?> 
                                
                            </small>
                        </h3>
                        
                        <ul class="list-inline">
                            <?php
                                if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
                                {   
                                    echo "<li><a href='mailto:robertgaleatfc@gmail.com'><i class='fas fa-envelope emailbutton'></i></a><br>
                                        </li><br>";
                                    $query = "SELECT emailAddress, contactNumber FROM tbl_staffmember WHERE staffMemberId = 2";
                                    $result = mysqli_query($conn, $query)
                                    or die("Error in query: ". mysqli_error($conn));

                                    while($row = mysqli_fetch_array($result))
                                    {    
                                    echo "<small class='defaultfontbold'>".$row['emailAddress'] . "<br>" . "+365 " .$row['contactNumber']."</small>";

                                    }
                                    

                                }
                            ?>
                            
                            
                            
                            
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 text-center">
                    <div class="thumbnail">
                        <?php
                        $query = "SELECT image FROM tbl_staffmember WHERE staffMemberId = 3;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));    
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo '<img class="img-responsive" alt="" style="border-top-right-radius: 5px;border-top-left-radius: 5px" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                            }
                    ?>
                        <div class="caption">
                        <h3 class="defaultfontbold">
                            <?php 
                            
                            $query = "SELECT * FROM tbl_staffmember WHERE staffMemberId = 3;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['name']." ".$row['surname'];

                            } ?>
                            
                            <br>
                            <small>
                                
                            <?php                          
                            $query = "SELECT staffOccupationName FROM tbl_staffoccupation INNER JOIN tbl_staffmember ON tbl_staffmember.staffOccupationId = tbl_staffoccupation.staffOccupationId WHERE staffMemberId = 3";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['staffOccupationName'];

                            }?> 
                                
                            </small>
                        </h3>
                        
                        <ul class="list-inline">
                            <?php
                                if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
                                {   
                                    echo "<li><a href='mailto:robertgaleatfc@gmail.com'><i class='fas fa-envelope emailbutton'></i></a><br>
                                        </li><br>";
                                    $query = "SELECT emailAddress, contactNumber FROM tbl_staffmember WHERE staffMemberId = 3";
                                    $result = mysqli_query($conn, $query)
                                    or die("Error in query: ". mysqli_error($conn));

                                    while($row = mysqli_fetch_array($result))
                                    {    
                                    echo "<small class='defaultfontbold'>".$row['emailAddress'] . "<br>" . "+365 " .$row['contactNumber']."</small>";

                                    }
                                    

                                }
                            ?>
                            
                            
                            
                            
                        </ul>
                    </div>
                    </div>
                </div>
        
            <div class="col-md-4 text-center">
                    <div class="thumbnail">
                        <?php
                        $query = "SELECT image FROM tbl_staffmember WHERE staffMemberId = 4;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));    
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo '<img class="img-responsive" alt="" style="border-top-right-radius: 5px;border-top-left-radius: 5px" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                            }
                    ?>
                        <div class="caption">
                        <h3 class="defaultfontbold">
                            <?php 
                            
                            $query = "SELECT * FROM tbl_staffmember WHERE staffMemberId = 4;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['name']." ".$row['surname'];

                            } ?>
                            
                            <br>
                            <small>
                                
                            <?php                          
                            $query = "SELECT staffOccupationName FROM tbl_staffoccupation INNER JOIN tbl_staffmember ON tbl_staffmember.staffOccupationId = tbl_staffoccupation.staffOccupationId WHERE staffMemberId = 4";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['staffOccupationName'];

                            }?> 
                                
                            </small>
                        </h3>
                        
                        <ul class="list-inline">
                            <?php
                                if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
                                {   
                                    echo "<li><a href='mailto:robertgaleatfc@gmail.com'><i class='fas fa-envelope emailbutton'></i></a><br>
                                        </li><br>";
                                    $query = "SELECT emailAddress, contactNumber FROM tbl_staffmember WHERE staffMemberId = 4";
                                    $result = mysqli_query($conn, $query)
                                    or die("Error in query: ". mysqli_error($conn));

                                    while($row = mysqli_fetch_array($result))
                                    {    
                                    echo "<small class='defaultfontbold'>".$row['emailAddress'] . "<br>" . "+365 " .$row['contactNumber']."</small>";

                                    }
                                }
                            ?>

                        </ul>
                    </div>
                    </div>
                </div>
        
            <div class="col-md-4 text-center">
                    <div class="thumbnail">
                        <?php
                        $query = "SELECT image FROM tbl_staffmember WHERE staffMemberId = 5;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));    
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo '<img class="img-responsive" alt="" style="border-top-right-radius: 5px;border-top-left-radius: 5px" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                            }
                    ?>
                        <div class="caption">
                        <h3 class="defaultfontbold">
                            <?php 
                            
                            $query = "SELECT * FROM tbl_staffmember WHERE staffMemberId = 5;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['name']." ".$row['surname'];

                            } ?>
                            
                            <br>
                            <small>
                                
                            <?php                          
                            $query = "SELECT staffOccupationName FROM tbl_staffoccupation INNER JOIN tbl_staffmember ON tbl_staffmember.staffOccupationId = tbl_staffoccupation.staffOccupationId WHERE staffMemberId = 5";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['staffOccupationName'];

                            }?> 
                                
                            </small>
                        </h3>
                        
                        <ul class="list-inline">
                            <?php
                                if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
                                {   
                                    echo "<li><a href='mailto:robertgaleatfc@gmail.com'><i class='fas fa-envelope emailbutton'></i></a><br>
                                        </li><br>";
                                    $query = "SELECT emailAddress, contactNumber FROM tbl_staffmember WHERE staffMemberId = 5";
                                    $result = mysqli_query($conn, $query)
                                    or die("Error in query: ". mysqli_error($conn));

                                    while($row = mysqli_fetch_array($result))
                                    {    
                                    echo "<small class='defaultfontbold'>".$row['emailAddress'] . "<br>" . "+365 " .$row['contactNumber']."</small>";

                                    }
                                }
                            ?>
                            
                            
                            
                            
                        </ul>
                    </div>
                    </div>
                </div>
        
            <div class="col-md-4 text-center">
                    <div class="thumbnail">
                        <?php
                        $query = "SELECT image FROM tbl_staffmember WHERE staffMemberId = 6;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));    
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo '<img class="img-responsive" alt="" style="border-top-right-radius: 5px;border-top-left-radius: 5px" src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'"/>';
                            }
                    ?>
                        <div class="caption">
                        <h3 class="defaultfontbold">
                            <?php 
                            
                            $query = "SELECT * FROM tbl_staffmember WHERE staffMemberId = 6;";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['name']." ".$row['surname'];

                            } ?>
                            
                            <br>
                            <small>
                                
                            <?php                          
                            $query = "SELECT staffOccupationName FROM tbl_staffoccupation INNER JOIN tbl_staffmember ON tbl_staffmember.staffOccupationId = tbl_staffoccupation.staffOccupationId WHERE staffMemberId = 6";
                            $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                            
                            while($row = mysqli_fetch_array($result))
                            {    
                            echo $row['staffOccupationName'];

                            }?> 
                                
                            </small>
                        </h3>
                        
                        <ul class="list-inline">
                            <?php
                                if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
                                {   
                                    echo "<li><a href='mailto:robertgaleatfc@gmail.com'><i class='fas fa-envelope emailbutton'></i></a><br>
                                        </li><br>";
                                    $query = "SELECT emailAddress, contactNumber FROM tbl_staffmember WHERE staffMemberId = 6";
                                    $result = mysqli_query($conn, $query)
                                    or die("Error in query: ". mysqli_error($conn));

                                    while($row = mysqli_fetch_array($result))
                                    {    
                                    echo "<small class='defaultfontbold'>".$row['emailAddress'] . "<br>" . "+365 " .$row['contactNumber']."</small>";

                                    }
                                }
                            ?>

                        </ul>
                    </div>
                    </div>
                </div>
        </div>
    </div>
        <!-- /.row -->
        
   
 
    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>