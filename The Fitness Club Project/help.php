<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
   
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">User Help
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <!-- /.row -->

        <div class="row" margin-top: -10px;>
            <div class="col-lg-12">
                <div class="jumbotronhelp">
                    <h1>Index</h1>
                    <h4>Please follow through the index navigation links to browse the website:</h4>
                    <ul>
                        <li>
                            <p>Landing Page <a href="index.php" style="text-decoration: none; color:#000000"><b>🡺 Home</b></a></p>
                        </li>
                        <li>
                            <p>General Information <a href="about.php" style="text-decoration: none; color:#000000"><b>🡺 About Us</b></a></p>
                        </li>
                         <li>
                            <p>Membership Prices <a href="prices.php" style="text-decoration: none; color:#000000"><b>🡺 Memberships</b></a></p>
                        </li>
                         <li>
                            <p>Gym Images <a href="gallery.php" style="text-decoration: none; color:#000000"><b>🡺 Gallery</b></a></p>
                        </li>
                         <li>
                            <p>Our Personal Trainers and Staff <a href="trainers.php" style="text-decoration: none; color:#000000"><b>🡺 Team</b></a></p>
                        </li>
                        <li>
                            <p>Our Gym Location <a href="location.php" style="text-decoration: none; color:#000000"><b>🡺 Location</b></a></p>
                        </li>
                        <li>
                            <p>Send us a message <a href="contact.php" style="text-decoration: none; color:#000000"><b>🡺 Contact Us</b></a></p>
                        </li>
                            <p>Take a look at our fitness partners at<a href="https://eurosport.com.mt" style="text-decoration: none; color:#000000"><b> eurosport.com.mt</b></a></p>
                    </ul>
                </div>
            </div>
        </div>

        
        
    </div>

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>