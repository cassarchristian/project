
        <div class="row"style="margin-top: 20px;">
            <div class="col-lg-12">
                <h4 class="page-header defaultfontbold">Apply via Online Form
                    <br><small class="defaultfont">Fill in the form below to apply with The Fitness Club</small>
                </h4>
            </div>
            
            <div class="col-md-10">
                <form id="contact-form" method="post" action="application.php" role="form" enctype="multipart/form-data">

                <div class="messages"></div>

                <div class="controls defaultfont">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form_name">First Name *</label>
                                <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your name *" required="required" data-error="First Name is required.">
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form_lastname">Last Name *</label>
                                <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your surname *" required="required" data-error="Last Name is required.">
                                
                            </div>
                        </div>
                        
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form_phone">Date of Birth *</label>
                                <input id="form_phone" type="date" name="dob" class="form-control" placeholder="Please enter your age *"
                                required="required" data-error="Age is required.">
                                
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="form_email">House Number *</label>
                                <input id="form_email" type="text" name="hnumber" class="form-control" placeholder="Enter number" required="required">
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form_email">House Address *</label>
                                <input id="form_email" type="text" name="haddress" class="form-control" placeholder="Please enter your house address *" required="required">
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form_email">Street Address *</label>
                                <input id="form_email" type="text" name="saddress" class="form-control" placeholder="Please enter your street address *" required="required">
                                
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="sel1">Location *</label>
                                  <select class="form-control" name="location" id="sel1">
                                      <?php
                                            


                                            $query = "SELECT * FROM tbl_locality";
                                            
                                            $result = mysqli_query($conn, $query)
                                            or die("Error in query: ". mysqli_error($conn));
                                      
                                            while($row = mysqli_fetch_array($result))
                                            {    
                                            echo '<option value='.$row['localityId'].'>'.$row['localityName'].'</option>';
                                            }



                                        ?>
                                      
                                  </select>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form_phone">Phone Number *</label>
                                <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your contact number *"
                                required="required" data-error="Phone Number is required.">
                                
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                          <label for="sel1">Membership Type *</label>
                          <select class="form-control" name="membershiptype" id="sel1">
                            <?php
                                             


                                            $query = "SELECT * FROM tbl_membershiptype ORDER BY membershipTypeId";
                                            
                                            $result = mysqli_query($conn, $query)
                                            or die("Error in query: ". mysqli_error($conn));
                                      
                                            while($row = mysqli_fetch_array($result))
                                            {    
                                            echo '<option value='.$row['membershipTypeId'].'>'.$row['typeName'].'</option>';
                                            }



                                        ?>
                          </select>
                        </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <label for="form_name">Username *</label>
                                <input id="form_name" type="text" name="username" class="form-control" placeholder="Please enter a username *" required="required">
                        </div>
                        <div class="col-md-4">
                            <label for="form_name">Password *</label>
                                <input id="form_name" type="password" name="password" class="form-control" placeholder="Please enter a password *" required="required">
                                <small style="font-size: 10px;">Password must be longer than 6 characters!</small>
                        </div>
                        <div class="col-md-4">
                            <label for="form_name">Confirm Password *</label>
                                <input id="form_name" type="password" name="conpassword" class="form-control" placeholder="Please confirm your password *" required="required">
                            <small style="font-size: 10px;">Password must match with the previous field!</small>
                        </div>
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4" style="margin-top: 10px;">
                            <label for="form_name">Profile Image </label>
                                <input class="form-control" type="file" name="profileimage" id="fileToUpload" accept="image/*">
                        </div>
                    </div>
                    
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12">
                            <input type="submit" name="submit" class="btn btn-send btn-sm defaultbutton" style="width: 20%;"value="Apply">
                        </div>
                    </div>
                    </div>
            </form>
            </div>
            
            <div class="col-lg-12">
                <h4 class="page-header defaultfontbold">Apply via Printed Form
                    <br><small class="defaultfont">Please download the printable form below and submit it to the gym</small><br>
                </h4>
                <i class="fas fa-file-alt" style="font-size: 15px;"></i><a href="extra/form.pdf" class="defaultfontbold" style="text-decoration: none; color:#000000;font-size: 15px;" download> Download</a>
            </div>
            
        </div>
    
        

    
    <?php

                
                    if( isset($_POST['submit'])){
                        $name = mysqli_real_escape_string($conn,trim($_POST['name']));
                        $surname = mysqli_real_escape_string($conn,trim($_POST['surname']));
                        $dob = mysqli_real_escape_string($conn,trim($_POST['dob']));
                        $houseNo = mysqli_real_escape_string($conn,trim($_POST['hnumber']));
                        $houseAddress = mysqli_real_escape_string($conn,trim($_POST['haddress']));
                        $streetAddress = mysqli_real_escape_string($conn,trim($_POST['saddress']));
                        $location = $_POST['location'];
                        $phoneNumber = mysqli_real_escape_string($conn,trim($_POST['phone']));
                        $email = mysqli_real_escape_string($conn,trim($_POST['email']));
                        $limitsize=4294967295;
                        
                        if($_FILES['profileimage']['size'] != 0){
                            $image = addslashes(file_get_contents($_FILES['profileimage']['tmp_name'])); //SQL Injection defence!
                            $imagesize=$_FILES['profileimage']['size'];
                        }else{
                            $image = null;
                            $imagesize = 0;
                        }
                        
                        
                        
                        
                        
                        $membershiptype = $_POST['membershiptype'];
                        $username = mysqli_real_escape_string($conn,trim($_POST['username']));
                        $password = sha1($_POST['password']);
                        $passwordnothashed = $_POST['password'];
                        $conpassword = sha1($_POST['conpassword']);
                        $dateRegistered = date("Y-m-d");
                        
                        
                        
                        
                        
                        if(is_numeric($name)){
                            echo "<div class='container'>
                                    <div class='row'>
                                    <h3 class='defaultfontbold'>Error: Message could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>First name is numeric, please try again!<p>
                                    </div>
                                  </div>";

                        }
                        else if(is_numeric($surname)){
                            echo "<div class='container'>
                                    <div class='row'>
                                    <h3 class='defaultfontbold'>Error: Message could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>Last name is numeric, please try again!<p>
                                    </div>
                                  </div>";

                        }

                        
                        else if(!is_numeric($houseNo)){
                            echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Form could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>Door Number is invalid, please try again!<p>
                                    </div>
                                  </div>";
                        }                    
                        else if(!is_numeric($phoneNumber) || strlen($phoneNumber) != 8){
                            echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Form could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>Phone Number is invalid, please try again!<p>
                                    </div>
                                  </div>";
                        }
                        else if(strlen($password) < 7){
                            echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Form could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>Password must be longer than 6 characters!<p>
                                    </div>
                                  </div>";
                        }
                        else if($password != $conpassword){
                            echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Form could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>Passwords did not match, please try again!<p>
                                    </div>
                                  </div>";
                        }
                        
                        else if($imagesize > $limitsize){
                            echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Form could not be sent.</h3>
                                    <p class='defaultfontbold' style='color: red'>Profile image size is too large!<p>
                                    </div>
                                  </div>";
                        }
                        else{
                            $emailCheckQuery = "select COUNT(*) from tbl_member where emailAddress LIKE '$email'";
                            $emailCheckresult = mysqli_query($conn, $emailCheckQuery)
                                    or die("Error in query: ". mysqli_error($conn));
                            $emailRow = mysqli_fetch_row($emailCheckresult);
                            $emailcount = $emailRow[0];
                            
                            
                            $usernameCheckQuery = "select COUNT(*) from tbl_member where username LIKE '$username'";
                            $usernameCheckresult = mysqli_query($conn, $usernameCheckQuery)
                                    or die("Error in query: ". mysqli_error($conn));
                            $usernameRow = mysqli_fetch_row($usernameCheckresult);
                            $usernamecount = $usernameRow[0];
                            
                            
                            
                            
                            if($emailcount > 0){
                                echo "<div class='container'>
                                            <div class='row' style='margin-top: 40px;'>
                                            <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Message could not be sent.</h3>
                                            <p class='defaultfontbold' style='color: red'>An account is already assosciated with this email, please try again!<p>
                                            </div>
                                          </div>";
                            }
                            
                            else if($usernamecount > 0){
                                echo "<div class='container'>
                                            <div class='row' style='margin-top: 40px;'>
                                            <h3 class='defaultfontbold'><i class='fas fa-exclamation-triangle' style='margin-right: 10px;'></i>Error: Form could not be sent.</h3>
                                            <p class='defaultfontbold' style='color: red'>Username already exists, please try again!<p>
                                            </div>
                                          </div>";
                            }
                            
                           
                            else{ // if condition ends here if it is new entry, echo will work
                                    $query = "INSERT INTO tbl_member (name, surname, dateOfBirth, houseNo, houseAddress, streetAddress,
                                    contactNumber, emailAddress, image, dateRegistered, gymId, localityId, membershipTypeId, username, password) VALUES ('$name', '$surname', '$dob','$houseNo','$houseAddress','$streetAddress','$phoneNumber','$email','{$image}','$dateRegistered','1','$location','$membershiptype','$username','$password')";

                                    $result = mysqli_query($conn, $query)
                                    or die("Error in query: ". mysqli_error($conn));
                                    
                                    $to = $email;
                                    $subject = 'The Fitness Club - Registration';
                                    $message = 'Upon your registration, you are kindly to submit your payment by hand at our opening hours at the gym.' . "\n\n" . "Enjoy your membership access such as ongoing events and nutrition plans through our online website - thefitnessclub.com.mt" . "\n" . "\n" . "Login Details" . "\n" . "Username: " . $username . "\n" . "Password: " . $passwordnothashed;
                                    $headers = 'From: thefitnesscluborg@gmail.com' . "\r\n" .
                                        'Reply-To:'. $email . "\r\n";
                                    mail($to, $subject, $message, $headers);
                                    sleep(2);
                                    echo "<meta http-equiv='refresh' content=\"0; url=applicationsent.php\">";
                                    exit();
                                }
                        

                            
                        }
                        

                    }

                ?>
