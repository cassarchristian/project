<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
  
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">About
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img class="img-responsive" src="images/about.png" style="border-radius: 6px;"alt="">
            </div>
            <div class="col-md-6">
                <h2 class="defaultfontbold">Experienced in Fitness</h2>
                <p class="defaultfont">For over 15 years, The Fitness Club has been one of the local leaders in wellness, fitness and the management of fitness centers and leisure facilities. </p>
                <p class="defaultfont">We pride ourselves as the leading innovators, by being the first to offer free personal training, physiotherapists as per of our teams, free group classes, and advocating training ethics within all our outlets.</p>
                <p class="defaultfont">Our Fitness Centre is highly rated and recommended by the islands most successful atheletes!</p>
                <p class="defaultfont"><b><a href=trainers.php style="text-decoration: none; color:#000000">Our Team</a></b> is ready to help you achieve your goals!</p>
            </div>
        </div>

        <div class="row" style="background-color: white; margin-top: 30px;">
            <div class="col-md-3 col-sm-6">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <span class="fa-stack fa-5x">
                                 <i class="fa fa-circle fa-stack-2x text-primary" style="color: #FF1010"></i>
                                 <i class="fas fa-angle-double-right fa-stack-1x" style="color: #FFFFFF"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <h4 class="panelhead">Progress</h4>
                            <p class="defaultfont">We make sure you gain your maximum results</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <span class="fa-stack fa-5x">
                                  <i class="fa fa-circle fa-stack-2x text-primary" style="color: #FF1010"></i>
                                 <i class="fas fa-comments fa-stack-1x" style="color: #FFFFFF"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <h4 class="panelhead">Communication</h4>
                            <p class="defaultfont">Continuous help and guide is given to our members</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <span class="fa-stack fa-5x">
                                  <i class="fa fa-circle fa-stack-2x text-primary" style="color: #FF1010"></i>
                                 <i class="far fa-calendar-alt fa-stack-1x" style="color: #FFFFFF"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <h4 class="panelhead">Events</h4>
                            <p class="defaultfont">We organize fitness events targeting all ages</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <span class="fa-stack fa-5x">
                                   <i class="fa fa-circle fa-stack-2x text-primary" style="color: #FF1010"></i>
                                 <i class="fas fa-users fa-stack-1x" style="color: #FFFFFF"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <h4 class="panelhead">Professional Trainers</h4>
                            <p class="defaultfont">Our trainers will guide you to workout routines</p>
                        </div>
                    </div>
                </div>
        </div>

        <div class="well">
            <div class="row">
                <div class="col-md-8">
                    <p class="defaultfont">Our gym is equipped with the best fitness equipment, machines, lockers and different areas.
                        Including cardio & muscle building zones, crossfit and yoga areas!
                    </p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-default btn-block defaultfontbold" href="gallery.php">View Gallery</a>
                </div>
            </div>
        </div>
    </div>

    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>
