<?php 

session_start();

?>
<!DOCTYPE html>
<html lang="en" style="background-color: white;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The Fitness Club - Health & Fitness Club</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fitnessclub.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
</head>

<body style="background-color: white;">
    
        
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'navbarlogged.php';

            }else{

                include 'navbar.php';

            }
        ?>
    
    
    
    
    
    
    <div class="container">
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header defaultfontbold">Application
                    <br><small class="defaultfont">The Fitness Club</small>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <img class="img-responsive" src="images/application.png" style="border-top-left-radius: 7px; border-top-right-radius: 7px;" alt="">
            </div>
        </div>
        
        <?php
            if(!isset($_SESSION['valid_user']) && !isset($_SESSION['valid_pass']))
            {
                include 'applicationform.php';

            }else{

                echo "<div class='container'>
                                    <div class='row' style='margin-top: 40px;'>
                                    <h3 class='defaultfontbold'><i class='fas fa-user-times' style='margin-right: 10px;'></i>Error: You are already a member.</h3>
                                    <p class='defaultfontbold' style='color: red'>You can not sign up again!<p>
                                    </div>
                                  </div>";
                echo "<p class='defaultfont' style='margin-bottom: 100px'><b><a href=userinfo.php style='text-decoration: none; color:#000000'>View Membership Info</a></b></p>";

            }
        ?>
        

        
    </div>
    
    
        <?php
            if(isset($_SESSION['valid_user']) && isset($_SESSION['valid_pass']))
            {
                include 'footerlogged.php';

            }else{

                include 'footer.php';

            }
        ?>
    
    
    
    
    
    
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>