
    <footer>
         <div class="container">
           <div class="row">

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <img src="images/logo.png" class="navbar-brand" style="height: 100px;">
                    </div>

                    <div class="col-md-2 col-sm-3 col-xs-12">
                        <ul class="menu">
                             <span>BROWSE</span>    
                             <li>
                                <a href="index.php">Home</a>
                              </li>

                              <li>
                                 <a href="about.php">About</a>
                              </li>
                                <li>
                                 <a href="prices.php">Memberships</a>
                              </li>
                                <li>
                                 <a href="gallery.php">Gallery</a>
                              </li>
                                <li>
                                 <a href="trainers.php">Team</a>
                              </li>
                              <li>
                                 <a href="location.php">Location</a>
                              </li>
                              <li>
                                 <a href="events.php">Events</a>
                              </li>
                              <li>
                                <a href="videos.php">Videos</a>
                              </li>

                              <li>
                                 <a href="contact.php">Contact Us</a>
                              </li>
                         </ul>
                    </div>
               
               
                    <div class="col-md-2 col-sm-3 col-xs-12">
                        <ul class="menu">
                             <span>ACCESS</span>
                                <li>
                                 <a href="userinfo.php">Membership</a>
                              </li>
                              <li>
                                 <a href="events.php">Events</a>
                              </li>
                                <li>
                                 <a href="videos.php">Videos</a>
                              </li>
                            <li>
                                 <a href="fitnessnews.php">Fitness News</a>
                              </li>

                                <li>
                                 <a href="nutrition.php">Nutrition</a>
                              </li>
                              <li>
                                 <a href="reportfault.php">Report Equipment</a>
                              </li>

                         </ul>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                      <ul class="address">
                            <span>Contact</span>       
                            <li>
                               <i class="fa fa-phone" aria-hidden="true" style="color: #FF1010;"></i><a href="contact.php"> +365 79123456</a>
                            </li>
                            <li>
                               <i class="fa fa-map-marker" aria-hidden="true" style="color: #FF1010;"></i><a href="location.php"> The Fitness Club, Fleur De Lys, Birkirkara</a>
                            </li> 
                            <li>
                               <i class="fa fa-envelope" aria-hidden="true" style="color: #FF1010;"></i> <a href="contact.php"> thefitnesscluborg@gmail.com</a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-square" aria-hidden="true" style="color: #FF1010; margin-right: 10px;"></i></a>
                                <a href="https://www.twitter.com" target="_blank"><i class="fab fa-twitter-square" aria-hidden="true" style="color: #FF1010; margin-right: 10px;"></i></a>
                                <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram" aria-hidden="true" style="color: #FF1010;margin-right: 10px;"></i></a>
                                <a href="help.php"><i class="fas fa-question-circle" aria-hidden="true" style="color: #FF1010; margin-right: 10px;"></i></a>
                            </li>



                       </ul>
                   </div>


               </div> 
            </div>
        </footer>
