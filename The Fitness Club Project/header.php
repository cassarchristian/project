<header id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image: url(images/landing01.jpg)"></div>
                
                <div class="carousel-caption">
                            <h2 class="quotes">"Work today, achieve tomorrow"</h2>
                            <a href="prices.php" class="landinglink"><b>JOIN NOW</b></a>
                </div>        
            </div>
            <div class="item">
                <div class="fill" style="background-image:url(images/landing02.jpg);"></div>
                <div class="carousel-caption">
                    <h2 class="quotes">"Your only limit is yourself"</h2>
                    <a href="trainers.php" class="landinglink"><b>OUR TEAM</b></a>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url(images/landing03.jpg);"></div>
                <div class="carousel-caption">
                    <h2 class="quotes">"Be stronger than your excuses""</h2>
                    <a href="contact.php" class="landinglink"><b>CONTACT US</b></a>
                </div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>