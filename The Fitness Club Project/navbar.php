     <?php
        require_once("conn.php");
        $conn = connectDB();              
    ?>
        <nav class="navbar navbar-inverse navbar-fixed-top" id="notloggednav" role="navigation" style="box-shadow: 0px 0px 47px 6px rgba(0,0,0,0.55);">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.php" class="navbar-left"><img id="logo" src="images/logo.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="navbarlink" href="about.php">ABOUT US</a>
                        </li>
                        <li>
                            <a class="navbarlink" href="prices.php">MEMBERSHIPS</a>
                        </li>
                        <li>
                            <a class="navbarlink" href="trainers.php">TEAM</a>
                        </li>
                        <li>
                            <a class="navbarlink" href="contact.php">CONTACT</a>
                        </li>
                        
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle navbarlink" data-toggle="dropdown">LOGIN<span class="caret"></span></a>
                            <ul id="login-dp" class="dropdown-menu">
                                <li>
                                     <div class="row">
                                            <div class="col-md-12">
                                                <p class="navbarlink">Login to your Fitness Club Account</p>

                                                 <form class="form defaultfont" role="form" method="post" action="login.php" accept-charset="UTF-8" id="login-nav">
                                                        <div class="form-group">
                                                             <label class="sr-only" for="exampleInputEmail2">Username</label>
                                                             <input type="text" class="form-control" placeholder="Username" name="username" required>
                                                        </div>
                                                        <div class="form-group">
                                                             <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                             <input type="password" class="form-control" placeholder="Password" name="password" required>
                                                        </div>
                                                        <div class="form-group">
                                                             <button type="submit" name="submit" class="btn btn-block defaultbutton">Sign In</button>  
                                                        </div>
                                                        
                                                 </form>
                                            </div>
                                            <div class="bottom text-center defaultfont">
                                                <b><a href="forgotpassword.php" style="text-decoration: none; color: black;">Forgot your password? </a></b>
                                            </div>
                                            <div class="bottom text-center defaultfont">
                                                <b>Would like to join? <a href="prices.php" style="text-decoration: none; color: black;">Apply Now</a></b>
                                            </div>
                                     </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
