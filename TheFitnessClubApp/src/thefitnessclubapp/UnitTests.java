/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thefitnessclubapp;

/**
 *
 * @author cassa
 */
public class UnitTests {
    public int membershipMonths(String membershipType){
        int membershipDuration;
        if(membershipType.toLowerCase() == "beginner"){
            membershipDuration = 1;
        }
        else if(membershipType.toLowerCase() == "intermediate"){
            membershipDuration = 3;
        }
        else if(membershipType.toLowerCase() == "advanced"){
            membershipDuration = 6;
        }
        else if(membershipType.toLowerCase() == "fitlife"){
            membershipDuration = 12;
        }else{
            membershipDuration = 0;
        }

        return membershipDuration;
    }
    
    public String[] LoginDetails(String username, String password){
        String[] details = {username, password};
        
        return details;
        
    }
    
    public Boolean checkAdmin(String username){
        if(username.toLowerCase().matches("admin")){
            return true;
        }else{
            return false;
        }
    }
    
    public Object generatedUserName(String firstName, String lastName, String memberId){
        String username = firstName.substring(0,2) + lastName.substring(0,2) + memberId;
        return username;  
    }
}
