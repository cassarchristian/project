/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thefitnessclubapp;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author cassa
 */
@Entity
@Table(name = "tbl_member", catalog = "thefitnessclub_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "TblMember.findAll", query = "SELECT t FROM TblMember t")
    , @NamedQuery(name = "TblMember.findByMemberId", query = "SELECT t FROM TblMember t WHERE t.memberId = :memberId")
    , @NamedQuery(name = "TblMember.findByName", query = "SELECT t FROM TblMember t WHERE t.name = :name")
    , @NamedQuery(name = "TblMember.findBySurname", query = "SELECT t FROM TblMember t WHERE t.surname = :surname")
    , @NamedQuery(name = "TblMember.findByDateOfBirth", query = "SELECT t FROM TblMember t WHERE t.dateOfBirth = :dateOfBirth")
    , @NamedQuery(name = "TblMember.findByHouseNo", query = "SELECT t FROM TblMember t WHERE t.houseNo = :houseNo")
    , @NamedQuery(name = "TblMember.findByHouseAddress", query = "SELECT t FROM TblMember t WHERE t.houseAddress = :houseAddress")
    , @NamedQuery(name = "TblMember.findByStreetAddress", query = "SELECT t FROM TblMember t WHERE t.streetAddress = :streetAddress")
    , @NamedQuery(name = "TblMember.findByContactNumber", query = "SELECT t FROM TblMember t WHERE t.contactNumber = :contactNumber")
    , @NamedQuery(name = "TblMember.findByEmailAddress", query = "SELECT t FROM TblMember t WHERE t.emailAddress = :emailAddress")
    , @NamedQuery(name = "TblMember.findByDateRegistered", query = "SELECT t FROM TblMember t WHERE t.dateRegistered = :dateRegistered")
    , @NamedQuery(name = "TblMember.findByGymId", query = "SELECT t FROM TblMember t WHERE t.gymId = :gymId")
    , @NamedQuery(name = "TblMember.findByLocalityId", query = "SELECT t FROM TblMember t WHERE t.localityId = :localityId")
    , @NamedQuery(name = "TblMember.findByMembershipTypeId", query = "SELECT t FROM TblMember t WHERE t.membershipTypeId = :membershipTypeId")
    , @NamedQuery(name = "TblMember.findByUsername", query = "SELECT t FROM TblMember t WHERE t.username = :username")
    , @NamedQuery(name = "TblMember.findByPassword", query = "SELECT t FROM TblMember t WHERE t.password = :password")})
public class TblMember implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "memberId")
    private Integer memberId;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "surname")
    private String surname;
    @Basic(optional = false)
    @Column(name = "dateOfBirth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @Basic(optional = false)
    @Column(name = "houseNo")
    private int houseNo;
    @Basic(optional = false)
    @Column(name = "houseAddress")
    private String houseAddress;
    @Basic(optional = false)
    @Column(name = "streetAddress")
    private String streetAddress;
    @Basic(optional = false)
    @Column(name = "contactNumber")
    private int contactNumber;
    @Basic(optional = false)
    @Column(name = "emailAddress")
    private String emailAddress;
    @Basic(optional = false)
    @Lob
    @Column(name = "image")
    private byte[] image;
    @Basic(optional = false)
    @Column(name = "dateRegistered")
    @Temporal(TemporalType.DATE)
    private Date dateRegistered;
    @Basic(optional = false)
    @Column(name = "gymId")
    private int gymId;
    @Basic(optional = false)
    @Column(name = "localityId")
    private int localityId;
    @Basic(optional = false)
    @Column(name = "membershipTypeId")
    private int membershipTypeId;
    @Basic(optional = false)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;

    public TblMember() {
    }

    public TblMember(Integer memberId) {
        this.memberId = memberId;
    }

    public TblMember(Integer memberId, String name, String surname, Date dateOfBirth, int houseNo, String houseAddress, String streetAddress, int contactNumber, String emailAddress, byte[] image, Date dateRegistered, int gymId, int localityId, int membershipTypeId, String username, String password) {
        this.memberId = memberId;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.houseNo = houseNo;
        this.houseAddress = houseAddress;
        this.streetAddress = streetAddress;
        this.contactNumber = contactNumber;
        this.emailAddress = emailAddress;
        this.image = image;
        this.dateRegistered = dateRegistered;
        this.gymId = gymId;
        this.localityId = localityId;
        this.membershipTypeId = membershipTypeId;
        this.username = username;
        this.password = password;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        Integer oldMemberId = this.memberId;
        this.memberId = memberId;
        changeSupport.firePropertyChange("memberId", oldMemberId, memberId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        String oldSurname = this.surname;
        this.surname = surname;
        changeSupport.firePropertyChange("surname", oldSurname, surname);
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        Date oldDateOfBirth = this.dateOfBirth;
        this.dateOfBirth = dateOfBirth;
        changeSupport.firePropertyChange("dateOfBirth", oldDateOfBirth, dateOfBirth);
    }

    public int getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(int houseNo) {
        int oldHouseNo = this.houseNo;
        this.houseNo = houseNo;
        changeSupport.firePropertyChange("houseNo", oldHouseNo, houseNo);
    }

    public String getHouseAddress() {
        return houseAddress;
    }

    public void setHouseAddress(String houseAddress) {
        String oldHouseAddress = this.houseAddress;
        this.houseAddress = houseAddress;
        changeSupport.firePropertyChange("houseAddress", oldHouseAddress, houseAddress);
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        String oldStreetAddress = this.streetAddress;
        this.streetAddress = streetAddress;
        changeSupport.firePropertyChange("streetAddress", oldStreetAddress, streetAddress);
    }

    public int getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(int contactNumber) {
        int oldContactNumber = this.contactNumber;
        this.contactNumber = contactNumber;
        changeSupport.firePropertyChange("contactNumber", oldContactNumber, contactNumber);
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        String oldEmailAddress = this.emailAddress;
        this.emailAddress = emailAddress;
        changeSupport.firePropertyChange("emailAddress", oldEmailAddress, emailAddress);
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        byte[] oldImage = this.image;
        this.image = image;
        changeSupport.firePropertyChange("image", oldImage, image);
    }

    public Date getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(Date dateRegistered) {
        Date oldDateRegistered = this.dateRegistered;
        this.dateRegistered = dateRegistered;
        changeSupport.firePropertyChange("dateRegistered", oldDateRegistered, dateRegistered);
    }

    public int getGymId() {
        return gymId;
    }

    public void setGymId(int gymId) {
        int oldGymId = this.gymId;
        this.gymId = gymId;
        changeSupport.firePropertyChange("gymId", oldGymId, gymId);
    }

    public int getLocalityId() {
        return localityId;
    }

    public void setLocalityId(int localityId) {
        int oldLocalityId = this.localityId;
        this.localityId = localityId;
        changeSupport.firePropertyChange("localityId", oldLocalityId, localityId);
    }

    public int getMembershipTypeId() {
        return membershipTypeId;
    }

    public void setMembershipTypeId(int membershipTypeId) {
        int oldMembershipTypeId = this.membershipTypeId;
        this.membershipTypeId = membershipTypeId;
        changeSupport.firePropertyChange("membershipTypeId", oldMembershipTypeId, membershipTypeId);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        String oldUsername = this.username;
        this.username = username;
        changeSupport.firePropertyChange("username", oldUsername, username);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        String oldPassword = this.password;
        this.password = password;
        changeSupport.firePropertyChange("password", oldPassword, password);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (memberId != null ? memberId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblMember)) {
            return false;
        }
        TblMember other = (TblMember) object;
        if ((this.memberId == null && other.memberId != null) || (this.memberId != null && !this.memberId.equals(other.memberId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "thefitnessclubapp.TblMember[ memberId=" + memberId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
