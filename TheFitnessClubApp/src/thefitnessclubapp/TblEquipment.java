/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thefitnessclubapp;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author cassa
 */
@Entity
@Table(name = "tbl_equipment", catalog = "thefitnessclub_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "TblEquipment.findAll", query = "SELECT t FROM TblEquipment t")
    , @NamedQuery(name = "TblEquipment.findByEquipmentId", query = "SELECT t FROM TblEquipment t WHERE t.equipmentId = :equipmentId")
    , @NamedQuery(name = "TblEquipment.findByName", query = "SELECT t FROM TblEquipment t WHERE t.name = :name")
    , @NamedQuery(name = "TblEquipment.findByFaultStatus", query = "SELECT t FROM TblEquipment t WHERE t.faultStatus = :faultStatus")
    , @NamedQuery(name = "TblEquipment.findByGymId", query = "SELECT t FROM TblEquipment t WHERE t.gymId = :gymId")})
public class TblEquipment implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "equipmentId")
    private Integer equipmentId;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "faultStatus")
    private boolean faultStatus;
    @Basic(optional = false)
    @Column(name = "gymId")
    private int gymId;

    public TblEquipment() {
    }

    public TblEquipment(Integer equipmentId) {
        this.equipmentId = equipmentId;
    }

    public TblEquipment(Integer equipmentId, String name, boolean faultStatus, int gymId) {
        this.equipmentId = equipmentId;
        this.name = name;
        this.faultStatus = faultStatus;
        this.gymId = gymId;
    }

    public Integer getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Integer equipmentId) {
        Integer oldEquipmentId = this.equipmentId;
        this.equipmentId = equipmentId;
        changeSupport.firePropertyChange("equipmentId", oldEquipmentId, equipmentId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public boolean getFaultStatus() {
        return faultStatus;
    }

    public void setFaultStatus(boolean faultStatus) {
        boolean oldFaultStatus = this.faultStatus;
        this.faultStatus = faultStatus;
        changeSupport.firePropertyChange("faultStatus", oldFaultStatus, faultStatus);
    }

    public int getGymId() {
        return gymId;
    }

    public void setGymId(int gymId) {
        int oldGymId = this.gymId;
        this.gymId = gymId;
        changeSupport.firePropertyChange("gymId", oldGymId, gymId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (equipmentId != null ? equipmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblEquipment)) {
            return false;
        }
        TblEquipment other = (TblEquipment) object;
        if ((this.equipmentId == null && other.equipmentId != null) || (this.equipmentId != null && !this.equipmentId.equals(other.equipmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "thefitnessclubapp.TblEquipment[ equipmentId=" + equipmentId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
