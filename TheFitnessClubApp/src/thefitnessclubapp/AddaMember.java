/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thefitnessclubapp;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import java.io.*;
import java.security.MessageDigest;
/**
 *
 * @author cassa
 */
public class AddaMember extends javax.swing.JFrame {
public GregorianCalendar cal = new GregorianCalendar();
public boolean isLeapYear;
public File f;
    /**
     * Creates new form AddaMember
     */
    public AddaMember() {
        initComponents();
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("favicon.png")));
        try{
            Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/thefitnessclub_db","root","");
            ArrayList<String> localityNames = new ArrayList<String>();
            ArrayList<String> localityIds = new ArrayList<String>();
            String query = "SELECT localityId, localityName FROM tbl_locality;"; 
            PreparedStatement stm = conn.prepareStatement(query); 
            
            ResultSet rs = stm.executeQuery(query); 

            while (rs.next()) { 
                String localityName = rs.getString("localityName");
                String localityId = rs.getString("localityId");
                
                
                String fullLocality = localityId + " - " + localityName;
                localityNames.add(fullLocality);
                localityIds.add(localityId);
            } 

            rs.close(); 
            // Populate the combo box
            DefaultComboBoxModel model = new DefaultComboBoxModel(localityNames.toArray());
            cmbLocation.setModel(model);
            
        }catch(Exception e){
            System.out.println(e);
        }
        
        
        try{
            Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/thefitnessclub_db","root","");
            ArrayList<String> typeNames = new ArrayList<String>();
            ArrayList<String> typeIds = new ArrayList<String>();
            String query = "SELECT membershipTypeId, typeName FROM tbl_membershiptype ORDER BY membershipTypeId;"; 
            PreparedStatement stm = conn.prepareStatement(query); 
            
            ResultSet rs = stm.executeQuery(query); 

            while (rs.next()) { 
                String membershipTypeName = rs.getString("typeName");
                String membershipTypeId = rs.getString("membershipTypeId");
                
                String fullmembershipType = membershipTypeId + " - " + membershipTypeName;
                
                typeNames.add(fullmembershipType);
                typeIds.add(membershipTypeId);

                String[] parts = fullmembershipType.split("-");
                String id = parts[0]; 
                
                
            } 

            rs.close(); 
            // Populate the combo box
            DefaultComboBoxModel model = new DefaultComboBoxModel(typeNames.toArray());
            cmbMembershipType.setModel(model);
            
        }catch(Exception e){
            System.out.println(e);
        }
        
        
        
        
        
        
        ArrayList<String> years_tmp = new ArrayList<String>();
        for(int years = 1970; years<=Calendar.getInstance().get(Calendar.YEAR); years++) {
            years_tmp.add(years+"");
        }
        
        DefaultComboBoxModel model = new DefaultComboBoxModel(years_tmp.toArray());
        cmbYear.setModel(model);
        
        
        for(int i = 1; i <= 12;i++){
            cmbMonth.addItem(""+i+"");
        }
        
    }
    
    public int membershipMonths(String membershipType){
        int membershipDuration;
        if(membershipType.toLowerCase() == "beginner"){
            membershipDuration = 1;
        }
        else if(membershipType.toLowerCase() == "intermediate"){
            membershipDuration = 3;
        }
        else if(membershipType.toLowerCase() == "advanced"){
            membershipDuration = 6;
        }
        else if(membershipType.toLowerCase() == "fitlife"){
            membershipDuration = 12;
        }else{
            membershipDuration = 0;
        }

        return membershipDuration;
    }
    
    public String[] LoginDetails(String username, String password){
        String[] details = {username, password};
        
        return details;
        
    }
    
    public Boolean checkAdmin(String username){
        if(username.toLowerCase().matches("admin")){
            return true;
        }else{
            return false;
        }
    }
    
    public Object generatedUserName(String firstName, String lastName, String memberId){
        String username = firstName.substring(0,2) + lastName.substring(0,2) + memberId;
        return username;  
    }
        

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator4 = new javax.swing.JSeparator();
        jPasswordField1 = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtSurname = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        cmbMonth = new javax.swing.JComboBox<>();
        cmbYear = new javax.swing.JComboBox<>();
        cmbDay = new javax.swing.JComboBox<>();
        txtHouseNumber = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtHouseAddress = new javax.swing.JTextField();
        txtStreetAddress = new javax.swing.JTextField();
        cmbLocation = new javax.swing.JComboBox<>();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        txtPhoneNumber = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel15 = new javax.swing.JLabel();
        cmbMembershipType = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        btnAddMember = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        passPassword = new javax.swing.JPasswordField();
        passConPassword = new javax.swing.JPasswordField();
        btnPicture = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        lblProfilePic = new javax.swing.JLabel();

        jPasswordField1.setText("jPasswordField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Add a Member - The Fitness Club Management Application");

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Add Member - The Fitness Club Management Application");
        jLabel1.setToolTipText("");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Basic Information");

        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });

        jLabel7.setText("Name");

        jLabel8.setText("Surname");

        txtSurname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSurnameActionPerformed(evt);
            }
        });

        jLabel20.setText("Date of Birth");

        cmbMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMonthActionPerformed(evt);
            }
        });

        cmbYear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbYearActionPerformed(evt);
            }
        });

        txtHouseNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHouseNumberActionPerformed(evt);
            }
        });

        jLabel9.setText("House Number");

        jLabel10.setText("House Address");

        jLabel11.setText("Street Address");

        jLabel12.setText("Location");

        jLabel2.setText("Phone Number");

        jLabel4.setText("Email");

        jLabel15.setText("Membership Type");

        cmbMembershipType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel16.setText("Username");

        txtUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsernameActionPerformed(evt);
            }
        });

        jLabel17.setText("Password");

        jLabel19.setText("Confirm Password");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Address Information");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Contact Information");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("Membership Information");

        btnAddMember.setText("Add Member");
        btnAddMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMemberActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        passConPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passConPasswordActionPerformed(evt);
            }
        });

        btnPicture.setText("Upload Profile Picture");
        btnPicture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPictureActionPerformed(evt);
            }
        });

        jLabel14.setText("Month");

        jLabel18.setText("Day");

        jLabel21.setText("Year");

        lblProfilePic.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProfilePic.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtHouseNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtHouseAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(38, 357, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtStreetAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(46, 46, 46)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmbLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(40, 40, 40)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 670, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 670, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 670, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(86, 86, 86)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel2))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(129, 129, 129)
                                    .addComponent(jLabel5))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(129, 129, 129)
                                    .addComponent(jLabel6))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(129, 129, 129)
                                        .addComponent(jLabel13))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(70, 70, 70)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel17)
                                            .addComponent(jLabel16)
                                            .addComponent(jLabel15)
                                            .addComponent(jLabel19))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(passConPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(cmbMembershipType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                                            .addComponent(passPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btnPicture, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(lblProfilePic, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(85, 85, 85)
                                        .addComponent(btnAddMember)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnCancel))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(115, 115, 115)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtSurname, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel7)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabel3)))
                                .addGap(38, 38, 38)
                                .addComponent(jLabel20)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtSurname, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(cmbMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtHouseNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtHouseAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbLocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtStreetAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12))
                    .addComponent(jLabel11))
                .addGap(21, 21, 21)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbMembershipType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(passPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(passConPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblProfilePic, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnPicture)
                            .addComponent(btnAddMember)
                            .addComponent(btnCancel))))
                .addContainerGap(71, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNameActionPerformed

    private void txtSurnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSurnameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSurnameActionPerformed

    private void txtHouseNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHouseNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHouseNumberActionPerformed

    private void txtUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUsernameActionPerformed

    private void passConPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passConPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passConPasswordActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void cmbYearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbYearActionPerformed
        // TODO add your handling code here:
        isLeapYear = cal.isLeapYear(Integer.parseInt(cmbYear.getSelectedItem().toString()));
    }//GEN-LAST:event_cmbYearActionPerformed

    private void cmbMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbMonthActionPerformed
        // TODO add your handling code here:
        int monthIndex = cmbMonth.getSelectedIndex();
        if(monthIndex == 0 || monthIndex == 2 || monthIndex == 4 || monthIndex == 6 || monthIndex == 7 || monthIndex == 9 || monthIndex == 11){
            cmbDay.removeAllItems();
            for(int i = 1; i <= 31; i++){
                cmbDay.addItem(i+"");
            }
            
        }
        else if(monthIndex == 3 || monthIndex == 5 || monthIndex == 8 || monthIndex == 10){
            cmbDay.removeAllItems();
            for(int i = 1; i <= 30; i++){
                cmbDay.addItem(i+"");
            }
        }
        else{
            if(isLeapYear){ //if the year is a leap year, february will have 29 days 
                cmbDay.removeAllItems();
                for(int i = 1; i <= 29; i++){
                        cmbDay.addItem(i+"");
                        }
            }
            else{ //if not leap year, february will have 28 days as usual
                cmbDay.removeAllItems();
                for(int i = 1; i <= 28; i++){
                        cmbDay.addItem(i+"");
                        }  
            }
        }
    }//GEN-LAST:event_cmbMonthActionPerformed

    private void btnPictureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPictureActionPerformed
        // TODO add your handling code here:
        
        String imagePath = "";
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        f = chooser.getSelectedFile();
        try{
            imagePath = f.getAbsolutePath();
        
        }catch(NullPointerException e){
            
        }
        
        try{
            Image bi = ImageIO.read(f);
            Image dimg = bi.getScaledInstance(lblProfilePic.getWidth(), lblProfilePic.getHeight(),Image.SCALE_SMOOTH);
            ImageIcon imageIcon = new ImageIcon(dimg);
            lblProfilePic.setIcon(imageIcon);
        }
        catch(Exception e){
            
        }
        //ImageIcon icon = new ImageIcon(imagePath);
        //lblProfilePic.setIcon(icon);
    }//GEN-LAST:event_btnPictureActionPerformed

    private void btnAddMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMemberActionPerformed
        // TODO add your handling code here:
        String name = txtName.getText();
        String lastname = txtSurname.getText();
        String dobyear = cmbYear.getSelectedItem().toString();
        String dobmonth = cmbMonth.getSelectedItem().toString();
        String dobday = cmbDay.getSelectedItem().toString();
        String houseNumber = txtHouseNumber.getText();
        String houseAddress = txtHouseAddress.getText();
        String streetAddress = txtStreetAddress.getText();
        String dob = dobyear + "-" +  dobmonth + "-" + dobday;
        String location = cmbLocation.getSelectedItem().toString();
        String[] parts = location.split("-");
        String locationid = parts[0]; 
        
        
        String phoneNumber = txtPhoneNumber.getText();
        String email = txtEmail.getText();
        
        String membershiptype = cmbMembershipType.getSelectedItem().toString();
        String[] typeparts = membershiptype.split("-");
        String typeid = typeparts[0]; 
        
        
        String username = txtUsername.getText();
        
        String password = passPassword.getText();
        String confirmpassword = passConPassword.getText();
        
        Icon profilePic = lblProfilePic.getIcon();
        
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateToday = sdf.format(d);
        
        if(name.matches("[0-9]+") || (name.length() == 0)){
            JOptionPane.showMessageDialog(null,"Please input a filled out valid first name!","Warning",2);
        }else if(lastname.matches("[0-9]+") || (lastname.length() == 0)){
            JOptionPane.showMessageDialog(null,"Please input a filled out valid last name!","Warning",2);
        }
        else if(!houseNumber.matches("[0-9]+")){
            JOptionPane.showMessageDialog(null,"House number must be filled out and must be numeric!","Warning",2);
        }else if(houseAddress.length() == 0){
            JOptionPane.showMessageDialog(null,"House address must be filled out!","Warning",2);
        }
        else if(streetAddress.length() == 0){
            JOptionPane.showMessageDialog(null,"Street address must be filled out!","Warning",2);
        }
        else if(!phoneNumber.matches("[0-9]+")){
            JOptionPane.showMessageDialog(null,"Phone number must be filled out and  must be numeric!","Warning",2);
        }else if(phoneNumber.length() != 8){
            JOptionPane.showMessageDialog(null,"Phone number must be 8 characters long!","Warning",2);
        }else if(!email.contains("@")){
            JOptionPane.showMessageDialog(null,"Email field must contain '@'!","Warning",2);
        }
        else if(username.length() == 0){
            JOptionPane.showMessageDialog(null,"Username must be filled out!","Warning",2);
        }
        else if(password.length() == 0){
            JOptionPane.showMessageDialog(null,"Password must be filled out!","Warning",2);
        }
        else if(password.length() < 7){
            JOptionPane.showMessageDialog(null,"Password must be longer than 6 characters!","Warning",2);
        }
        else if(confirmpassword.length() == 0){
            JOptionPane.showMessageDialog(null,"Confirmation Password must be filled out!","Warning",2);
        }
       
        else if(!password.matches(confirmpassword)){
            JOptionPane.showMessageDialog(null,"Passwords do not match!","Warning",2);
        }else if(profilePic == null){
            try{
                
                
                byte[] input = password.getBytes();
                MessageDigest SHA1 = MessageDigest.getInstance("SHA1");
                SHA1.update(input);
                byte[] digest = SHA1.digest();
                
                StringBuffer hexDigest = new StringBuffer();
                for(int i = 0;i<digest.length;i++){
                    hexDigest.append(Integer.toString((digest[i]&0xff)+0x100,16).substring(1));
                }
                
                
                
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/thefitnessclub_db","root","");
                
                PreparedStatement pstmt = conn.prepareStatement("INSERT INTO tbl_member (name, surname, dateOfBirth, houseNo, houseAddress, streetAddress,contactNumber, emailAddress, dateRegistered, gymId, localityId, membershipTypeId, username, password) VALUES ('"+name+"','"+lastname+"','"+dob+"','"+houseNumber+"'"
                        + ",'"+houseAddress+"','"+streetAddress+"','"+phoneNumber+"','"+email+"','"+dateToday+"','1','"+locationid+"','"+typeid+"','"+username+"','"+hexDigest+"');");
                pstmt.executeUpdate();
                pstmt.close();
                
                JOptionPane.showMessageDialog(null,name + " " + lastname + " has been added!","Database Updated",1);
                
                conn.close();
                }catch(Exception e){ 
                    JOptionPane.showMessageDialog(null,e,"Error",1);
                }
            
                String subject = "The Fitness Club - Registration";
                String message = "Upon your registration, enjoy your membership access such as ongoing events and nutrition plans through our online website - thefitnessclub.com.mt" + "\n" + "Login Details" + "\n" + "Username: " + username + "\n" + "Password: " + password;

                String user = "thefitnesscluborg@gmail.com";
                String pass = "befitstayfit";

                SendMail.send(email,subject,message,user,pass);
                this.dispose();
            
            
        }else{
            try{
                
                byte[] input = password.getBytes();
                MessageDigest SHA1 = MessageDigest.getInstance("SHA1");
                SHA1.update(input);
                byte[] digest = SHA1.digest();
                
                StringBuffer hexDigest = new StringBuffer();
                for(int i = 0;i<digest.length;i++){
                    hexDigest.append(Integer.toString((digest[i]&0xff)+0x100,16).substring(1));
                }
                
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/thefitnessclub_db","root","");
                FileInputStream fis = new FileInputStream(f);
                PreparedStatement pstmt = conn.prepareStatement("INSERT INTO tbl_member (name, surname, dateOfBirth, houseNo, houseAddress, streetAddress,contactNumber, emailAddress, image, dateRegistered, gymId, localityId, membershipTypeId, username, password) VALUES ('"+name+"','"+lastname+"','"+dob+"','"+houseNumber+"'"
                        + ",'"+houseAddress+"','"+streetAddress+"','"+phoneNumber+"','"+email+"',?,'"+dateToday+"','1','"+locationid+"','"+typeid+"','"+username+"','"+hexDigest+"');");
                
                pstmt.setBinaryStream(1,(InputStream)fis,(int)f.length());
                pstmt.executeUpdate();
                pstmt.close();
                
                JOptionPane.showMessageDialog(null,name + " " + lastname + " has been added!","Database Updated",1);
                
                conn.close();
                }catch(Exception e){ 
                    JOptionPane.showMessageDialog(null,e,"Error",1);
                }
            
                String subject = "The Fitness Club - Registration";
                String message = "Upon your registration, enjoy your membership access such as ongoing events and nutrition plans through our online website - thefitnessclub.com.mt" + "\n\n" + "Login Details" + "\n" + "Username: " + username + "\n" + "Password: " + password;

                String user = "thefitnesscluborg@gmail.com";
                String pass = "befitstayfit";

                SendMail.send(email,subject,message,user,pass);
                this.dispose();
        }
        
        
        
        
    }//GEN-LAST:event_btnAddMemberActionPerformed
        
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddaMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddaMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddaMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddaMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddaMember().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddMember;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnPicture;
    private javax.swing.JComboBox<String> cmbDay;
    private javax.swing.JComboBox<String> cmbLocation;
    private javax.swing.JComboBox<String> cmbMembershipType;
    private javax.swing.JComboBox<String> cmbMonth;
    private javax.swing.JComboBox<String> cmbYear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JLabel lblProfilePic;
    private javax.swing.JPasswordField passConPassword;
    private javax.swing.JPasswordField passPassword;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtHouseAddress;
    private javax.swing.JTextField txtHouseNumber;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPhoneNumber;
    private javax.swing.JTextField txtStreetAddress;
    private javax.swing.JTextField txtSurname;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
