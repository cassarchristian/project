/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thefitnessclubapp;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author cassa
 */
@Entity
@Table(name = "tbl_staffmember", catalog = "thefitnessclub_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "TblStaffmember.findAll", query = "SELECT t FROM TblStaffmember t")
    , @NamedQuery(name = "TblStaffmember.findByStaffMemberId", query = "SELECT t FROM TblStaffmember t WHERE t.staffMemberId = :staffMemberId")
    , @NamedQuery(name = "TblStaffmember.findByName", query = "SELECT t FROM TblStaffmember t WHERE t.name = :name")
    , @NamedQuery(name = "TblStaffmember.findBySurname", query = "SELECT t FROM TblStaffmember t WHERE t.surname = :surname")
    , @NamedQuery(name = "TblStaffmember.findByDateOfBirth", query = "SELECT t FROM TblStaffmember t WHERE t.dateOfBirth = :dateOfBirth")
    , @NamedQuery(name = "TblStaffmember.findByHouseNo", query = "SELECT t FROM TblStaffmember t WHERE t.houseNo = :houseNo")
    , @NamedQuery(name = "TblStaffmember.findByHouseAddress", query = "SELECT t FROM TblStaffmember t WHERE t.houseAddress = :houseAddress")
    , @NamedQuery(name = "TblStaffmember.findByStreetAddress", query = "SELECT t FROM TblStaffmember t WHERE t.streetAddress = :streetAddress")
    , @NamedQuery(name = "TblStaffmember.findByContactNumber", query = "SELECT t FROM TblStaffmember t WHERE t.contactNumber = :contactNumber")
    , @NamedQuery(name = "TblStaffmember.findByEmailAddress", query = "SELECT t FROM TblStaffmember t WHERE t.emailAddress = :emailAddress")
    , @NamedQuery(name = "TblStaffmember.findByStaffOccupationId", query = "SELECT t FROM TblStaffmember t WHERE t.staffOccupationId = :staffOccupationId")
    , @NamedQuery(name = "TblStaffmember.findByLocalityId", query = "SELECT t FROM TblStaffmember t WHERE t.localityId = :localityId")
    , @NamedQuery(name = "TblStaffmember.findByGymId", query = "SELECT t FROM TblStaffmember t WHERE t.gymId = :gymId")})
public class TblStaffmember implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "staffMemberId")
    private Integer staffMemberId;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "surname")
    private String surname;
    @Basic(optional = false)
    @Column(name = "dateOfBirth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @Column(name = "houseNo")
    private Integer houseNo;
    @Basic(optional = false)
    @Column(name = "houseAddress")
    private String houseAddress;
    @Basic(optional = false)
    @Column(name = "streetAddress")
    private String streetAddress;
    @Basic(optional = false)
    @Column(name = "contactNumber")
    private int contactNumber;
    @Basic(optional = false)
    @Column(name = "emailAddress")
    private String emailAddress;
    @Basic(optional = false)
    @Lob
    @Column(name = "image")
    private byte[] image;
    @Basic(optional = false)
    @Column(name = "staffOccupationId")
    private int staffOccupationId;
    @Basic(optional = false)
    @Column(name = "localityId")
    private int localityId;
    @Basic(optional = false)
    @Column(name = "gymId")
    private int gymId;

    public TblStaffmember() {
    }

    public TblStaffmember(Integer staffMemberId) {
        this.staffMemberId = staffMemberId;
    }

    public TblStaffmember(Integer staffMemberId, String name, String surname, Date dateOfBirth, String houseAddress, String streetAddress, int contactNumber, String emailAddress, byte[] image, int staffOccupationId, int localityId, int gymId) {
        this.staffMemberId = staffMemberId;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.houseAddress = houseAddress;
        this.streetAddress = streetAddress;
        this.contactNumber = contactNumber;
        this.emailAddress = emailAddress;
        this.image = image;
        this.staffOccupationId = staffOccupationId;
        this.localityId = localityId;
        this.gymId = gymId;
    }

    public Integer getStaffMemberId() {
        return staffMemberId;
    }

    public void setStaffMemberId(Integer staffMemberId) {
        Integer oldStaffMemberId = this.staffMemberId;
        this.staffMemberId = staffMemberId;
        changeSupport.firePropertyChange("staffMemberId", oldStaffMemberId, staffMemberId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        String oldSurname = this.surname;
        this.surname = surname;
        changeSupport.firePropertyChange("surname", oldSurname, surname);
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        Date oldDateOfBirth = this.dateOfBirth;
        this.dateOfBirth = dateOfBirth;
        changeSupport.firePropertyChange("dateOfBirth", oldDateOfBirth, dateOfBirth);
    }

    public Integer getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(Integer houseNo) {
        Integer oldHouseNo = this.houseNo;
        this.houseNo = houseNo;
        changeSupport.firePropertyChange("houseNo", oldHouseNo, houseNo);
    }

    public String getHouseAddress() {
        return houseAddress;
    }

    public void setHouseAddress(String houseAddress) {
        String oldHouseAddress = this.houseAddress;
        this.houseAddress = houseAddress;
        changeSupport.firePropertyChange("houseAddress", oldHouseAddress, houseAddress);
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        String oldStreetAddress = this.streetAddress;
        this.streetAddress = streetAddress;
        changeSupport.firePropertyChange("streetAddress", oldStreetAddress, streetAddress);
    }

    public int getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(int contactNumber) {
        int oldContactNumber = this.contactNumber;
        this.contactNumber = contactNumber;
        changeSupport.firePropertyChange("contactNumber", oldContactNumber, contactNumber);
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        String oldEmailAddress = this.emailAddress;
        this.emailAddress = emailAddress;
        changeSupport.firePropertyChange("emailAddress", oldEmailAddress, emailAddress);
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        byte[] oldImage = this.image;
        this.image = image;
        changeSupport.firePropertyChange("image", oldImage, image);
    }

    public int getStaffOccupationId() {
        return staffOccupationId;
    }

    public void setStaffOccupationId(int staffOccupationId) {
        int oldStaffOccupationId = this.staffOccupationId;
        this.staffOccupationId = staffOccupationId;
        changeSupport.firePropertyChange("staffOccupationId", oldStaffOccupationId, staffOccupationId);
    }

    public int getLocalityId() {
        return localityId;
    }

    public void setLocalityId(int localityId) {
        int oldLocalityId = this.localityId;
        this.localityId = localityId;
        changeSupport.firePropertyChange("localityId", oldLocalityId, localityId);
    }

    public int getGymId() {
        return gymId;
    }

    public void setGymId(int gymId) {
        int oldGymId = this.gymId;
        this.gymId = gymId;
        changeSupport.firePropertyChange("gymId", oldGymId, gymId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (staffMemberId != null ? staffMemberId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblStaffmember)) {
            return false;
        }
        TblStaffmember other = (TblStaffmember) object;
        if ((this.staffMemberId == null && other.staffMemberId != null) || (this.staffMemberId != null && !this.staffMemberId.equals(other.staffMemberId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "thefitnessclubapp.TblStaffmember[ staffMemberId=" + staffMemberId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
