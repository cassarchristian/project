/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thefitnessclubapp;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author cassa
 */
public class UpdateaMember extends javax.swing.JFrame {
public File f;
    /**
     * Creates new form UpdateaMember
     */
    public UpdateaMember() {
        initComponents();
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("favicon.png")));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnUploadImg = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        lblChangePic = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtMemberId = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        passNewPass = new javax.swing.JPasswordField();
        passConNewPass = new javax.swing.JPasswordField();
        btnChangePass = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtNewUser = new javax.swing.JTextField();
        btnChangeUser = new javax.swing.JButton();
        btnChangePicture = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Update Members - The Fitness Club Management Application");

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Update Members - The Fitness Club Management Application");
        jLabel1.setToolTipText("");

        btnUploadImg.setText("Upload");
        btnUploadImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUploadImgActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Member Profile Settings");

        lblChangePic.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText("Member Id");

        jLabel4.setText("New Password");

        jLabel5.setText("Confirm New Password");

        passNewPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passNewPassActionPerformed(evt);
            }
        });

        passConNewPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passConNewPassActionPerformed(evt);
            }
        });

        btnChangePass.setText("Change Password");
        btnChangePass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangePassActionPerformed(evt);
            }
        });

        jLabel6.setText("New Username");

        txtNewUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNewUserActionPerformed(evt);
            }
        });

        btnChangeUser.setText("Change Username");
        btnChangeUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeUserActionPerformed(evt);
            }
        });

        btnChangePicture.setText("Change Picture");
        btnChangePicture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangePictureActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancel))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                                .addComponent(passNewPass, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(passConNewPass, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnChangePass, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(92, 92, 92)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNewUser, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnChangeUser, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(25, 25, 25))
            .addGroup(layout.createSequentialGroup()
                .addGap(237, 237, 237)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtMemberId))
                    .addComponent(lblChangePic, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14)
                    .addComponent(btnChangePicture, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                    .addComponent(btnUploadImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtMemberId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(lblChangePic, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUploadImg)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnChangePicture)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtNewUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChangeUser))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(passNewPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(passConNewPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChangePass))
                .addGap(18, 18, 18)
                .addComponent(btnCancel)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void passConNewPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passConNewPassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passConNewPassActionPerformed

    private void btnChangePassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangePassActionPerformed
        // TODO add your handling code here:
        String membershipId = txtMemberId.getText();
        String newPass = passNewPass.getText();
        String connewPass = passConNewPass.getText();
        
        if(membershipId.length() == 0){
            JOptionPane.showMessageDialog(null,"Please input a Member Id!","Warning",2);
        }else if(!membershipId.matches("[0-9]+")){
            JOptionPane.showMessageDialog(null,"Please input a valid Member Id!","Warning",2);
        }else if(newPass.length() == 0){
            JOptionPane.showMessageDialog(null,"Password field is empty!","Warning",2);
        }
        else if(newPass.length() < 7){
            JOptionPane.showMessageDialog(null,"Password must be more than 6 characters long!","Warning",2);
        }
        else if(connewPass.length() == 0){
            JOptionPane.showMessageDialog(null,"Confirmation password field is empty!","Warning",2);
        }else if(!newPass.matches(connewPass)){
            JOptionPane.showMessageDialog(null,"Passwords do not match!","Warning",2);
        }
        else{
            try{
                byte[] input = newPass.getBytes();
                MessageDigest SHA1 = MessageDigest.getInstance("SHA1");
                SHA1.update(input);
                byte[] digest = SHA1.digest();
                
                StringBuffer hexDigest = new StringBuffer();
                for(int i = 0;i<digest.length;i++){
                    hexDigest.append(Integer.toString((digest[i]&0xff)+0x100,16).substring(1));
                }
                
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/thefitnessclub_db","root","");
                PreparedStatement pstmt = conn.prepareStatement("UPDATE tbl_member SET password ='"+hexDigest+"' WHERE memberId ='"+membershipId+"';");
              
                pstmt.executeUpdate();
                pstmt.close();
                JOptionPane.showMessageDialog(null,"Password has been updated!","Database Updated",1);
                conn.close();
                }catch(Exception e){ 
                    JOptionPane.showMessageDialog(null,e,"Error",1);
                }
        }
    }//GEN-LAST:event_btnChangePassActionPerformed

    private void txtNewUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNewUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNewUserActionPerformed

    private void btnUploadImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUploadImgActionPerformed
        // TODO add your handling code here:
        String imagePath = "";
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        f = chooser.getSelectedFile();
        try{
            imagePath = f.getAbsolutePath();
        
        }catch(NullPointerException e){
            
        }
        
        try{
            Image bi = ImageIO.read(f);
            Image dimg = bi.getScaledInstance(lblChangePic.getWidth(), lblChangePic.getHeight(),Image.SCALE_SMOOTH);
            ImageIcon imageIcon = new ImageIcon(dimg);
            lblChangePic.setIcon(imageIcon);
        }
        catch(Exception e){
            
        }
    }//GEN-LAST:event_btnUploadImgActionPerformed

    private void btnChangePictureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangePictureActionPerformed
        // TODO add your handling code here:
        
        Icon newPic = lblChangePic.getIcon();
        String membershipId = txtMemberId.getText();
        if(membershipId.length() == 0){
            JOptionPane.showMessageDialog(null,"Please input a Member Id!","Warning",2);
        }else if(!membershipId.matches("[0-9]+")){
            JOptionPane.showMessageDialog(null,"Please input a valid Member Id!","Warning",2);
        }else if(newPic == null){
            JOptionPane.showMessageDialog(null,"No Profile Picture has been selected!","Warning",2);
        }
        else{
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/thefitnessclub_db","root","");
                FileInputStream fis = new FileInputStream(f);
                PreparedStatement pstmt = conn.prepareStatement("UPDATE tbl_member SET image = ? WHERE memberId ='"+membershipId+"';");
                
                pstmt.setBinaryStream(1,(InputStream)fis,(int)f.length());
                pstmt.executeUpdate();
                pstmt.close();
                
                JOptionPane.showMessageDialog(null,"Image has been updated for the selected member!","Database Updated",1);
                conn.close();
                }catch(Exception e){ 
                    JOptionPane.showMessageDialog(null,e,"Error",1);
                }
        }
        
    }//GEN-LAST:event_btnChangePictureActionPerformed

    private void btnChangeUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeUserActionPerformed
        // TODO add your handling code here:
        String membershipId = txtMemberId.getText();
        String newUsername = txtNewUser.getText();
        
        if(membershipId.length() == 0){
            JOptionPane.showMessageDialog(null,"Please input a Member Id!","Warning",2);
        }else if(!membershipId.matches("[0-9]+")){
            JOptionPane.showMessageDialog(null,"Please input a valid Member Id!","Warning",2);
        }else if(newUsername.length() == 0){
            JOptionPane.showMessageDialog(null,"Please input a new username!","Warning",2);
        }else{
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/thefitnessclub_db","root","");
                PreparedStatement pstmt = conn.prepareStatement("UPDATE tbl_member SET username ='"+newUsername+"' WHERE memberId ='"+membershipId+"';");
              
                pstmt.executeUpdate();
                pstmt.close();
                JOptionPane.showMessageDialog(null,"Username has been updated!","Database Updated",1);
                conn.close();
                }catch(Exception e){ 
                    JOptionPane.showMessageDialog(null,e,"Error",1);
                }
        }
        
    }//GEN-LAST:event_btnChangeUserActionPerformed

    private void passNewPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passNewPassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passNewPassActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpdateaMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpdateaMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpdateaMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpdateaMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UpdateaMember().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnChangePass;
    private javax.swing.JButton btnChangePicture;
    private javax.swing.JButton btnChangeUser;
    private javax.swing.JButton btnUploadImg;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel lblChangePic;
    private javax.swing.JPasswordField passConNewPass;
    private javax.swing.JPasswordField passNewPass;
    private javax.swing.JTextField txtMemberId;
    private javax.swing.JTextField txtNewUser;
    // End of variables declaration//GEN-END:variables
}
