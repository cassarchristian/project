/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thefitnessclubapp;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cassa
 */
public class AddaMemberTest {
    
    public AddaMemberTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of membershipMonths method, of class AddaMember.
     */
    @Test
    public void testMembershipMonths() {
        System.out.println("membershipMonths");
        String membershipType = "intermediate";
        AddaMember instance = new AddaMember();
        int expResult = 3;
        int result = instance.membershipMonths(membershipType);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of LoginDetails method, of class AddaMember.
     */
    @Test
    public void testLoginDetails() {
        System.out.println("LoginDetails");
        String username = "christian";
        String password = "12345678";
        AddaMember instance = new AddaMember();
        String[] expResult = {"christian","12345678"};
        String[] result = instance.LoginDetails(username, password);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkAdmin method, of class AddaMember.
     */
    @Test
    public void testCheckAdmin() {
        System.out.println("checkAdmin");
        String username = "nonadmin";
        AddaMember instance = new AddaMember();
        Boolean expResult = null;
        Boolean result = instance.checkAdmin(username);
        assertFalse(result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of generatedUserName method, of class AddaMember.
     */
    @Test
    public void testGeneratedUserName() {
        System.out.println("generatedUserName");
        String firstName = "Christian";
        String lastName = "Cassar";
        String memberId = "001";
        AddaMember instance = new AddaMember();
        Object expResult = null;
        Object result = instance.generatedUserName(firstName, lastName, memberId);
        assertNotNull(result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    
}
