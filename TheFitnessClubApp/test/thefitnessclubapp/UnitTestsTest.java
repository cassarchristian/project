/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thefitnessclubapp;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cassa
 */
public class UnitTestsTest {
    
    public UnitTestsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of membershipMonths method, of class UnitTests.
     */
    @Test
    public void testMembershipMonths() {
        System.out.println("membershipMonths");
        String membershipType = "intermediate";
        UnitTests instance = new UnitTests();
        int expResult = 3;
        int result = instance.membershipMonths(membershipType);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of LoginDetails method, of class UnitTests.
     */
    @Test
    public void testLoginDetails() {
        System.out.println("LoginDetails");
        String username = "christian";
        String password = "12345678";
        UnitTests instance = new UnitTests();
        String[] expResult = {"christian","12345678"};
        String[] result = instance.LoginDetails(username, password);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkAdmin method, of class UnitTests.
     */
    @Test
    public void testCheckAdmin() {
        System.out.println("checkAdmin");
        String username = "nonadmin";
        UnitTests instance = new UnitTests();
        Boolean result = instance.checkAdmin(username);
        assertFalse(result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of generatedUserName method, of class UnitTests.
     */
    @Test
    public void testGeneratedUserName() {
        System.out.println("generatedUserName");
        String firstName = "Christian";
        String lastName = "Cassar";
        String memberId = "001";
        UnitTests instance = new UnitTests();
        Object result = instance.generatedUserName(firstName, lastName, memberId);
        assertNotNull(result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
